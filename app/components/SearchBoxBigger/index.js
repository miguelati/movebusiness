import React from 'react';
import {Image} from 'react-native';
import {View, Input} from 'native-base';
import PropTypes from 'prop-types';
import styles from './styles';
import {colors} from '../../themes';

class SearchBoxBigger extends React.PureComponent {
  render() {
    const {onChange, placeholder} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.inputContainer}>
          <Image
            source={require('../../commons/img/buttons/search.png')}
            style={styles.searchIcon}
          />
          <Input
            onChangeText={onChange}
            style={styles.input}
            placeholder={placeholder}
            placeholderTextColor={colors.lightGrey}
          />
        </View>
      </View>
    );
  }
}

SearchBoxBigger.defaultProps = {
  value: '',
};

SearchBoxBigger.propTypes = {
  onChange: PropTypes.func.isRequired,
  value: PropTypes.string,
  placeholder: PropTypes.string,
};

export default SearchBoxBigger;
