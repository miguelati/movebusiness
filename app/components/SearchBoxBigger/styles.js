import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  container: {
    flex: 1,
  },
  inputContainer: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: colors.semiGrey,
    borderRadius: 15,
    marginLeft: 12,
    marginRight: 12,
    marginTop: 8,
    marginBottom: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchIcon: {
    marginLeft: 10,
  },
  input: {
    ...fonts.rubikLight14Black,
    marginLeft: 10,
  },
});
