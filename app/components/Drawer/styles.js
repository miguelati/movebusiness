import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  title: {
    ...fonts.rubikMedium32White,
    marginLeft: 20,
  },
  itemsContainer: {
    marginLeft: 0,
    justifyContent: 'flex-start',
  },
  iconContainer: {},
  label: {
    ...fonts.rubikLight18Blue,
    color: colors.white,
  },
  iconWhite: {
    color: colors.white,
    marginLeft: 20,
  },
  activeLabel: {
    ...fonts.rubikLight18Blue,
    color: colors.lightGrey,
  },
});
