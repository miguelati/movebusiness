import React from 'react';
import PropTypes from 'prop-types';
import {ScrollView, SafeAreaView} from 'react-native';
import {Grid, Row, Col, Text, Button} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {DrawerItems} from 'react-navigation-drawer';
import {withNavigation} from 'react-navigation';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';
import styles from './styles';
import I18n from '../../i18n';

class Drawer extends React.PureComponent {
  logoutHandler = async () => {
    const {navigation} = this.props;
    try {
      await AsyncStorage.removeItem('userCredential');
      await firebase.auth().signOut();
    } catch (error) {
      console.log('algo', error);
    } finally {
      navigation.navigate('Auth');
    }
  };

  goToView = view => {
    const {navigation} = this.props;
    navigation.navigate(view);
  };

  render() {
    return (
      <ScrollView style={styles.container}>
        <SafeAreaView forceInset={{top: 'always', horizontal: 'never'}}>
          <Grid>
            <Row>
              <Col>
                <Text style={styles.title}>{I18n.t('drawer.title')}</Text>
              </Col>
            </Row>
            {/* <Row>
              <Col>
                <DrawerItems
                  {...this.props}
                  labelStyle={styles.label}
                  activeLabelStyle={styles.activeLabel}
                  itemsContainerStyle={styles.itemsContainer}
                  iconContainerStyle={styles.iconContainer}
                />
              </Col>
            </Row> */}
            <Row>
              <Col>
                <Button
                  transparent
                  style={styles.itemsContainer}
                  onPress={() => this.goToView('Home')}>
                  <MaterialCommunityIcons
                    style={styles.iconWhite}
                    name="home"
                    size={30}
                  />
                  <Text style={styles.label}>
                    {I18n.t('drawer.items.home')}
                  </Text>
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  transparent
                  style={styles.itemsContainer}
                  onPress={() => this.goToView('Bussines')}>
                  <MaterialCommunityIcons
                    style={styles.iconWhite}
                    name="google-my-business"
                    size={30}
                  />
                  <Text style={styles.label}>
                    {I18n.t('drawer.items.bussines')}
                  </Text>
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  transparent
                  style={styles.itemsContainer}
                  onPress={() => this.goToView('Locals')}>
                  <MaterialIcons
                    style={styles.iconWhite}
                    name="location-on"
                    size={30}
                  />
                  <Text style={styles.label}>
                    {I18n.t('drawer.items.branchOffices')}
                  </Text>
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  transparent
                  style={styles.itemsContainer}
                  onPress={() => this.goToView('MenuOptionsList')}>
                  <MaterialCommunityIcons
                    style={styles.iconWhite}
                    name="silverware-variant"
                    size={30}
                  />
                  <Text style={styles.label}>
                    {I18n.t('drawer.items.menuOptions')}
                  </Text>
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>
                <Button
                  transparent
                  style={styles.itemsContainer}
                  onPress={this.logoutHandler}>
                  <MaterialCommunityIcons
                    style={styles.iconWhite}
                    name="logout"
                    size={30}
                  />
                  <Text style={styles.label}>
                    {I18n.t('drawer.items.logout')}
                  </Text>
                </Button>
              </Col>
            </Row>
          </Grid>
        </SafeAreaView>
      </ScrollView>
    );
  }
}

Drawer.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default withNavigation(Drawer);
