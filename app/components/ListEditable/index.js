import React from 'react';
import PropTypes from 'prop-types';
import {FlatList, TouchableOpacity} from 'react-native';
import {Row, Col, Text, Button, Icon} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';

class ListEditable extends React.PureComponent {
  renderItem = ({item}) => {
    const {onItemSelected} = this.props;
    const menuOptionIcon = item.iconName || 'food';
    return (
      <TouchableOpacity onPress={() => onItemSelected(item)}>
        <Row style={styles.itemRow}>
          <Col>
            <Row size={1}>
              <Col size={1} style={styles.center}>
                <Button transparent large>
                  <MaterialCommunityIcons
                    name={menuOptionIcon}
                    size={40}
                    style={styles.fontWhite}
                    type="MaterialCommunityIcons"
                  />
                </Button>
              </Col>
              <Col size={3} style={styles.textLeft}>
                <Text style={styles.item}>{item.name}</Text>
              </Col>
              <Col size={1} style={styles.center}>
                <Button transparent large>
                  <Icon name="add-circle" style={styles.item} type="Ionicons" />
                </Button>
              </Col>
            </Row>
            <Row style={styles.line} />
          </Col>
        </Row>
      </TouchableOpacity>
    );
  };

  render() {
    const {items} = this.props;

    return (
      <Row>
        <Col>
          <Row size={10}>
            <Col>
              <FlatList
                data={items}
                contentContainerStyle={styles.contentContainer}
                renderItem={this.renderItem}
                keyExtractor={item => item.id}
              />
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

ListEditable.propTypes = {
  items: PropTypes.array.isRequired,
  onItemSelected: PropTypes.func.isRequired,
};

export default ListEditable;
