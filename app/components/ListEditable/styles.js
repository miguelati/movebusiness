import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  item: {
    ...fonts.rubikMedium26White,
  },
  fontWhite: {
    color: 'white',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textLeft: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  itemRow: {
    height: 82,
  },
  inputAddItem: {
    marginLeft: 45,
  },
  line: {
    height: 1,
    backgroundColor: colors.line,
  },
});
