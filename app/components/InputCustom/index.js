import React from 'react';
import PropTypes from 'prop-types';
import {Item, Label, Input} from 'native-base';

class InputCustom extends React.PureComponent {
  render() {
    const {
      error,
      label,
      placeholder,
      value,
      handleChange,
      handleBlur,
      underlined,
    } = this.props;
    return (
      <Item
        stackedLabel
        error={error !== undefined}
        style={!underlined ? {borderBottomColor: 'transparent'} : null}>
        <Label>{label}</Label>
        <Input
          autoCapitalize="none"
          value={value}
          onChangeText={handleChange}
          onBlur={handleBlur}
          placeholder={placeholder}
        />
      </Item>
    );
  }
}

InputCustom.defaultProps = {
  error: undefined,
  underlined: true,
};

InputCustom.propTypes = {
  error: PropTypes.any,
  label: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  handleChange: PropTypes.func.isRequired,
  handleBlur: PropTypes.func.isRequired,
  underlined: PropTypes.bool,
};

export default InputCustom;
