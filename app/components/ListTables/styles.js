import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';
import {Dimensions} from 'react-native';
const {width} = Dimensions.get('window');

const equalWidth = width / 2 - 40;

export default EStyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    height: equalWidth,
    width: equalWidth,
    borderWidth: 1,
    borderColor: colors.yellow,
    borderRadius: 10,
    margin: 20,
  },
  itemTable: {
    ...fonts.rubikLight54Black,
  },
  itemChairs: {
    ...fonts.rubikRegular12Black,
  },
  itemSmoking: {
    ...fonts.rubikLight9Black,
  },
  itemTitle: {
    ...fonts.rubikRegular12Yellow,
  },
  textCenter: {
    justifyContent: 'center',
    textAlign: 'center',
  },
  textBottom: {
    textAlignVertical: 'bottom',
    textAlign: 'center',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  top: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  textName: {
    ...fonts.rubikRegular18Black,
  },
  textPermission: {
    ...fonts.rubikLight14Blue,
  },
});
