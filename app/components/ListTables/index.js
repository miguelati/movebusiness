import React from 'react';
import PropTypes from 'prop-types';
import {View, FlatList, TouchableOpacity} from 'react-native';
import {Row, Col, Text} from 'native-base';

import I18n from '../../i18n';
import styles from './styles';

class ListTables extends React.PureComponent {
  renderItem = ({item}) => {
    const {onItemPress} = this.props;
    return (
      <TouchableOpacity style={styles.itemBorder} onPres={onItemPress}>
        <Row style={styles.itemContainer} size={1}>
          <Col>
            <Row size={1}>
              <Col style={styles.bottom}>
                <Text style={[styles.itemTitle, styles.textBottom]}>
                  {I18n.t('tables.list.table')}
                </Text>
              </Col>
            </Row>
            <Row size={2}>
              <Col>
                <Text style={[styles.itemTable, styles.textCenter]}>
                  {item.get('tableNumber')}
                </Text>
              </Col>
            </Row>
            <Row size={1}>
              <Col>
                <Text style={[styles.itemChairs, styles.textCenter]}>
                  {I18n.t('tables.list.chairs', {
                    chairs: item.get('totalChairs'),
                  })}
                </Text>
              </Col>
            </Row>
            <Row size={1}>
              <Col>
                <Text style={[styles.itemSmoking, styles.textCenter]}>
                  {I18n.t('tables.list.smokingArea', {
                    can: item.get('smokingArea')
                      ? I18n.t('common.bool.yes')
                      : I18n.t('common.bool.no'),
                  })}
                </Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </TouchableOpacity>
    );
  };

  render() {
    const {items} = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={items}
          numColumns={2}
          contentContainerStyle={styles.contentContainer}
          renderItem={this.renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

ListTables.propTypes = {
  items: PropTypes.array.isRequired,
  onItemPress: PropTypes.func.isRequired,
};

export default ListTables;
