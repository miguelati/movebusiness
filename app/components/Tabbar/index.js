import React from 'react';
import PropTypes from 'prop-types';
import {FooterTab, Footer} from 'native-base';
import TabbarItem from './TabbarItem';

class Tabbar extends React.PureComponent {
  render() {
    const {navigationOptions} = this.props;
    return (
      <Footer>
        <FooterTab>
          <TabbarItem
            iconName="users"
            active={navigationOptions.navigation.state.index === 0}
            onPress={() => navigationOptions.navigation.navigate('Users')}
          />
          <TabbarItem
            iconName="menu"
            active={navigationOptions.navigation.state.index === 1}
            onPress={() => navigationOptions.navigation.navigate('Menu')}
          />
          <TabbarItem
            iconName="tables"
            active={navigationOptions.navigation.state.index === 2}
            onPress={() => navigationOptions.navigation.navigate('Tables')}
          />
        </FooterTab>
      </Footer>
    );
  }
}

Tabbar.propTypes = {
  navigationOptions: PropTypes.object.isRequired,
};

export default Tabbar;
