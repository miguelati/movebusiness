import React from 'react';
import PropTypes from 'prop-types';
import {Button} from 'native-base';
import {colors} from '../../themes';

import {createIconSetFromFontello} from 'react-native-vector-icons';
import fontelloConfig from '../../config/icons.json';
const Icon = createIconSetFromFontello(fontelloConfig);

class TabBarItem extends React.Component {
  render() {
    const {iconName, active, onPress} = this.props;

    const textColor = active ? colors.black : colors.grey;

    return (
      <Button vertical active={active} onPress={onPress}>
        <Icon style={{fontSize: 30, color: textColor}} name={iconName} />
      </Button>
    );
  }
}

TabBarItem.propTypes = {
  iconName: PropTypes.string.isRequired,
  active: PropTypes.bool.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default TabBarItem;
