import React from 'react';
import {View} from 'react-native';
import PropTypes from 'prop-types';

class BusinessInfo extends React.PureComponent {
  render() {
    return <View />;
  }
}

BusinessInfo.propTypes = {
  openHours: PropTypes.string,
};

export default BusinessInfo;
