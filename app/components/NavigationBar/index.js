import React from 'react';
import PropTypes from 'prop-types';
import {Header, Left, Body, Right, Button, Icon, Title} from 'native-base';
import {Image} from 'react-native';
import {withNavigation} from 'react-navigation';
import I18n from '../../i18n';
import {colors} from '../../themes';

class Navbar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      editing: false,
    };
  }
  backHandle = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  showMenuHandle = () => {
    const {navigation} = this.props;
    navigation.toggleDrawer();
  };

  filterHandle = () => {
    const {navigation} = this.props;
    navigation.navigate('Filter');
  };

  editHandle = () => {
    this.props.onEdit();
    this.setState(prevState => ({
      editing: !prevState.editing,
    }));
  };

  render() {
    const {
      editable,
      filter,
      withTitle,
      back,
      showMenu,
      title,
      transparent,
      barStyle,
    } = this.props;

    let barStyleOptions = {header: {}, buttons: {}, title: {}};
    let backButton = require('../../commons/img/buttons/back-yellow-button.png');
    if (barStyle === 'yellow') {
      barStyleOptions = {
        header: {backgroundColor: colors.yellow},
        buttons: {color: colors.white},
        title: {color: colors.white},
      };
      backButton = require('../../commons/img/buttons/back-white-button.png');
    } else if (barStyle === 'black') {
      barStyleOptions = {
        header: {backgroundColor: colors.black, marginHorizontal: 10},
        buttons: {color: colors.white},
        title: {color: colors.white},
      };
    } else {
      barStyleOptions = {
        header: {backgroundColor: colors.white, marginHorizontal: 10},
        buttons: {color: colors.black},
        title: {color: colors.black},
      };
    }

    if (transparent) {
      barStyleOptions.buttons = {color: colors.white};
    }

    return (
      <Header transparent={transparent} style={barStyleOptions.header}>
        <Left style={barStyle === 'yellow' ? {marginLeft: 10} : null}>
          {back && (
            <Button transparent onPress={this.backHandle}>
              <Image source={backButton} />
              {/* <Icon
                style={barStyleOptions.buttons}
                name="ios-arrow-back"
                type="Ionicons"
              />
              <Text style={barStyleOptions.buttons}>
                {I18n.t('navigationBar.buttons.back')}
              </Text> */}
            </Button>
          )}
          {showMenu && (
            <Button transparent onPress={this.showMenuHandle}>
              <Image
                source={require('../../commons/img/buttons/menu-black.png')}
              />
              {/* <Icon
                style={barStyleOptions.buttons}
                name="menu"
                type="Entypo"
              /> */}
            </Button>
          )}
        </Left>
        <Body>
          {withTitle && (
            <Title style={barStyleOptions.title}>
              {I18n.t(title, {defaultValue: title})}
            </Title>
          )}
        </Body>
        <Right style={barStyle === 'yellow' ? {marginRight: 10} : null}>
          {filter && (
            <Button transparent onPress={this.filterHandle}>
              <Icon name="md-options" type="Ionicons" />
            </Button>
          )}

          {editable && (
            <Button transparent onPress={this.editHandle}>
              {this.state.editing ? (
                <Icon
                  name="close"
                  type="FontAwesome"
                  style={{color: 'white'}}
                />
              ) : (
                <Icon name="edit" type="FontAwesome" style={{color: 'white'}} />
              )}
            </Button>
          )}
        </Right>
      </Header>
    );
  }
}

Navbar.defaultProps = {
  editable: false,
  onEdit: () => {},
  filter: false,
  withTitle: true,
  back: false,
  showMenu: false,
  title: 'navigationBar.title.home',
  navigation: {},
  transparent: false,
  barStyle: 'normal',
};

Navbar.propTypes = {
  editable: PropTypes.bool,
  onEdit: PropTypes.func,
  transparent: PropTypes.bool,
  barStyle: PropTypes.string,
  filter: PropTypes.bool,
  withTitle: PropTypes.bool,
  back: PropTypes.bool,
  showMenu: PropTypes.bool,
  title: PropTypes.string,
  navigation: PropTypes.object,
};

export default withNavigation(Navbar);
