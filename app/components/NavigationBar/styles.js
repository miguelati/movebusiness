import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  title: {
    ...fonts.rubikMedium32White,
    marginLeft: 40,
  },
  itemsContainer: {
    marginLeft: 40,
  },
  iconContainer: {},
  label: {
    ...fonts.rubikLight22White,
  },
  activeLabel: {
    ...fonts.rubikLight22White,
    color: colors.lightGrey,
  },
});
