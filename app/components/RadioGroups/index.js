import React from 'react';
import PropTypes from 'prop-types';
import {FlatList, TouchableOpacity} from 'react-native';
import {View, Text, Row, Col} from 'native-base';
import styles from './styles';
import RadioButton from '../RadioButton';

class RadioGroups extends React.PureComponent {
  constructor(props) {
    super(props);
    const {items, selected} = this.props;
    this.state = {
      selected: selected ? selected : items[0],
    };
  }

  onChanged = itemSelected => {
    const {onChanged} = this.props;
    this.setState({selected: itemSelected});
    onChanged(itemSelected);
  };

  renderItem = ({item}) => {
    const {selected} = this.state;
    return (
      <TouchableOpacity onPress={() => this.onChanged(item)}>
        <View style={styles.itemContainer}>
          <Row>
            <Col>
              <Text style={styles.text}>{item}</Text>
            </Col>
            <Col>
              <RadioButton selected={item === selected} />
            </Col>
          </Row>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const {items, style} = this.props;
    return (
      <View style={style}>
        <FlatList
          style={styles.flat}
          data={items}
          contentContainerStyle={styles.contentContainer}
          renderItem={this.renderItem}
          keyExtractor={item => `${item} - ${this.state.selected === item}`}
        />
      </View>
    );
  }
}

RadioGroups.defaultProps = {
  style: {},
  selected: null,
};

RadioGroups.propTypes = {
  style: PropTypes.object,
  onChanged: PropTypes.func.isRequired,
  items: PropTypes.array.isRequired,
  selected: PropTypes.string,
};

export default RadioGroups;
