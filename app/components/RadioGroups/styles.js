import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainer: {
    flexGrow: 1,
    alignSelf: 'stretch',
  },

  itemContainer: {
    height: 30,
  },
  text: {
    ...fonts.rubikRegular18Black,
  },
});
