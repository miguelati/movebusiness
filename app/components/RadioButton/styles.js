import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  border: {
    borderWidth: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
