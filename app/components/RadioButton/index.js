import React from 'react';
import PropTypes from 'prop-types';
import {View} from 'react-native';
import styles from './styles';
import {colors} from '../../themes';

class RadioButton extends React.PureComponent {
  constructor(props) {
    super(props);
    const {selected} = this.props;
    this.state = {
      selected: selected,
    };
  }

  onChanged = () => {
    this.setState(
      prevState => ({
        selected: !prevState.selected,
      }),
      () => {
        const {onChanged} = this.props;
        onChanged(this.state.selected);
      },
    );
  };

  render() {
    const {color, borderColor, size} = this.props;
    const {selected} = this.state;
    return (
      <View style={styles.container}>
        <View
          style={[
            styles.border,
            {
              width: size,
              height: size,
              borderRadius: size / 2,
              borderColor: colors[borderColor],
            },
          ]}>
          {selected && (
            <View
              style={[
                styles.dot,
                {
                  width: size - 6,
                  height: size - 6,
                  borderRadius: (size - 6) / 2,
                  backgroundColor: colors[color],
                },
              ]}
            />
          )}
        </View>
      </View>
    );
  }
}

RadioButton.defaultProps = {
  size: 20,
  borderColor: 'minorGrey',
  color: 'blue',
};

RadioButton.propTypes = {
  selected: PropTypes.bool.isRequired,
  color: PropTypes.string,
  borderColor: PropTypes.string,
  size: PropTypes.number,
};

export default RadioButton;
