import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  sectionContainer: {
    flex: 1,
    height: 52,
    backgroundColor: colors.white,
  },
  section: {
    flex: 1,
    height: 41,
    marginLeft: 44,
  },
  sectionText: {
    ...fonts.rubikRegular19Black,
    marginTop: 20,
  },
  line: {
    backgroundColor: colors.mediumGrey,
    height: 1,
  },
  itemContainer: {
    height: 90,
    flex: 1,
    backgroundColor: colors.white,
  },
  item: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 44,
  },
  itemLeft: {
    flex: 3,
    flexDirection: 'column',
  },
  itemRight: {
    flex: 1,
    justifyContent: 'center',
    alignContent: 'center',
  },
  itemImage: {
    width: 65,
    height: 65,
    borderRadius: 5,
  },
  itemProduct: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 3,
  },
  itemPrice: {
    flex: 1,
    justifyContent: 'flex-start',
    marginTop: 3,
  },
  itemProductText: {
    ...fonts.rubikRegular16Black,
  },
  itemPriceText: {
    ...fonts.rubikRegular16Yellow,
  },
  image: {
    width: 50,
    height: 50,
    borderRadius: 5,
  },
});
