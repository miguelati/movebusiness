import React from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, View, Text, SectionList, Image} from 'react-native';
import styles from './styles';

class ListMenu extends React.PureComponent {
  renderItem = ({item}) => {
    const {onItemSelected} = this.props;
    return (
      <TouchableOpacity
        style={styles.itemContainer}
        onPress={() => onItemSelected(item)}>
        <View style={styles.item}>
          <View style={styles.itemLeft}>
            <View style={styles.itemProduct}>
              <Text style={styles.itemProductText}>{item.product}</Text>
            </View>
            <View style={styles.itemPrice}>
              <Text style={styles.itemPriceText}>{item.price}</Text>
            </View>
          </View>
          <View style={styles.itemRight}>
            <Image
              style={styles.itemImage}
              source={
                item.pictureSmall
                  ? {uri: item.pictureSmall}
                  : require('../../commons/img/defaults/food-short.png')
              }
            />
          </View>
        </View>
        <View style={styles.line} />
      </TouchableOpacity>
    );
  };

  renderSectionHeader = ({section}) => {
    return (
      <View style={styles.sectionContainer}>
        <View style={styles.section}>
          <Text style={styles.sectionText}>{section.title}</Text>
        </View>
        <View style={styles.line} />
      </View>
    );
  };

  scrollToSection = item => {
    const {items} = this.props;
    const sections = items.map(section => section.title);
    const indexSection = sections.indexOf(item);
    this._sectionList.scrollToLocation({
      sectionIndex: indexSection,
      itemIndex: 0,
    });
  };

  render() {
    const {items} = this.props;
    return (
      <View>
        <SectionList
          ref={component => (this._sectionList = component)}
          style={styles.container}
          sections={items}
          renderItem={this.renderItem}
          renderSectionHeader={this.renderSectionHeader}
          keyExtractor={(item, index) => item + index}
        />
      </View>
    );
  }
}

ListMenu.propTypes = {
  items: PropTypes.array.isRequired,
};

export default ListMenu;
