import React from 'react';
import PropTypes from 'prop-types';
import {View, FlatList, Image, TouchableOpacity} from 'react-native';
import {Row, Col, Text} from 'native-base';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';

class ListLocals extends React.PureComponent {
  renderItem = ({item}) => {
    const {onItemPress} = this.props;
    const iconName = item.iconName || 'food';
    const image = item.pictureSmall
      ? {uri: item.pictureSmall}
      : require('../../commons/img/defaults/menuOption.png');
    return (
      <TouchableOpacity onPress={() => onItemPress(item)}>
        <Row style={styles.rowContainer}>
          <Col>
            <Row>
              <Col style={styles.center}>
                <MaterialCommunityIcons name={iconName} size={40} />
              </Col>
              <Col size={3} style={styles.centerLeft}>
                <Text style={styles.textName}>{item.name}</Text>
              </Col>
              <Col style={[styles.center, {marginRight: 20}]}>
                <Image source={image} style={styles.image} />
              </Col>
            </Row>
          </Col>
        </Row>
      </TouchableOpacity>
    );
  };

  render() {
    const {items} = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={items}
          contentContainerStyle={styles.contentContainer}
          renderItem={this.renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

ListLocals.propTypes = {
  items: PropTypes.array.isRequired,
  onItemPress: PropTypes.func.isRequired,
};

export default ListLocals;
