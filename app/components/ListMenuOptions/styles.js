import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  container: {
    flex: 1,
  },
  rowContainer: {
    height: 82,
    borderBottomWidth: 1,
    marginLeft: 20,
    borderBottomColor: colors.lightGrey,
  },
  image: {
    width: '80%',
    height: '70%',
    borderRadius: 5,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  centerLeft: {
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  bottom: {
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  top: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  textName: {
    ...fonts.rubikRegular18Black,
  },
  textPermission: {
    ...fonts.rubikLight14Blue,
  },
});
