import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  container: {
    flex: 1,
  },
  rowContainer: {
    height: 82,
    borderBottomWidth: 1,
    marginLeft: 20,
    borderBottomColor: colors.lightGrey,
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottom: {
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
  },
  top: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  image: {
    width: 65,
    height: 65,
    borderRadius: 32,
  },
  textName: {
    ...fonts.rubikRegular18Black,
  },
  textPermission: {
    ...fonts.rubikLight14Blue,
  },
});
