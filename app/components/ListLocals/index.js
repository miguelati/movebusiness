import React from 'react';
import PropTypes from 'prop-types';
import {View, FlatList, Image, TouchableOpacity} from 'react-native';
import {Row, Col, Text} from 'native-base';
import I18n from '../../i18n';
import styles from './styles';

class ListLocals extends React.PureComponent {
  renderItem = ({item}) => {
    const {onItemPress} = this.props;
    return (
      <TouchableOpacity onPress={() => onItemPress(item)}>
        <Row style={styles.rowContainer}>
          <Col>
            <Row>
              <Col style={styles.bottom}>
                <Text style={styles.textName}>{item.name}</Text>
              </Col>
            </Row>
            <Row>
              <Col style={styles.top}>
                <Text style={styles.textPermission}>{item.address}</Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </TouchableOpacity>
    );
  };

  render() {
    const {items} = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={items}
          contentContainerStyle={styles.contentContainer}
          renderItem={this.renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

ListLocals.propTypes = {
  items: PropTypes.array.isRequired,
  onItemPress: PropTypes.func.isRequired,
};

export default ListLocals;
