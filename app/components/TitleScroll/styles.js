import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  container: {
    flex: 1,
  },
  itemContainer: {
    flex: 1,
    width: 150,
    backgroundColor: colors.white,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    ...fonts.rubikRegular19Black,
  },
  textSelected: {
    ...fonts.rubikRegular19Black40,
  },
  line: {
    backgroundColor: colors.mediumGrey,
    height: 1,
  },
});
