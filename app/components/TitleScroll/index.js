import React from 'react';
import PropTypes from 'prop-types';
import {View, FlatList, Text, TouchableOpacity} from 'react-native';
import styles from './styles';

class TitleScroll extends React.PureComponent {
  itemWidth = 150;

  constructor(props) {
    super(props);
    const {items} = this.props;
    this.state = {
      selected: items[0],
    };
  }

  onPress = item => {
    const {onChange, items} = this.props;
    this.setState({selected: item});
    onChange(item);
    console.log(item);
    console.log(items.indexOf(item));
    const index = items.indexOf(item);
    this.flat.scrollToIndex({animated: true, index});
  };

  renderItem = ({item}) => {
    const {selected} = this.state;
    return (
      <TouchableOpacity onPress={() => this.onPress(item)}>
        <View style={styles.itemContainer}>
          <Text style={selected === item ? styles.textSelected : styles.text}>
            {item}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const {items} = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          ref={flat => (this.flat = flat)}
          data={items}
          renderItem={this.renderItem}
          horizontal
          keyExtractor={item => item}
          snapToAlignment="start"
          snapToInterval={this.itemWidth + 10}
          decelerationRate="fast"
          pagingEnabled
          showsHorizontalScrollIndicator={false}
        />
        <View style={styles.line} />
      </View>
    );
  }
}

TitleScroll.propTypes = {
  items: PropTypes.array.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default TitleScroll;
