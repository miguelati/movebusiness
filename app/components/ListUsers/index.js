import React from 'react';
import PropTypes from 'prop-types';
import {View, FlatList, Image, TouchableOpacity} from 'react-native';
import {Row, Col, Text} from 'native-base';
import I18n from '../../i18n';
import styles from './styles';

class ListUsers extends React.PureComponent {
  renderItem = ({item}) => {
    const {onItemPress} = this.props;
    return (
      <TouchableOpacity onPress={() => onItemPress(item)}>
        <Row style={styles.rowContainer}>
          <Col size={1} style={styles.center}>
            <Image
              style={styles.image}
              source={
                item.pictureSmall
                  ? {uri: item.pictureSmall}
                  : require('../../commons/img/defaults/user.png')
              }
            />
          </Col>
          <Col size={3}>
            <Row>
              <Col style={styles.bottom}>
                <Text style={styles.textName}>
                  {`${item.first_name} ${item.last_name}`}
                </Text>
              </Col>
            </Row>
            <Row>
              <Col style={styles.top}>
                <Text style={styles.textPermission}>
                  {I18n.t(`users.list.permissions.${item.permission}`)}
                </Text>
              </Col>
            </Row>
          </Col>
        </Row>
      </TouchableOpacity>
    );
  };

  render() {
    const {items} = this.props;
    return (
      <View style={styles.container}>
        <FlatList
          data={items}
          contentContainerStyle={styles.contentContainer}
          renderItem={this.renderItem}
          keyExtractor={item => item.id}
        />
      </View>
    );
  }
}

ListUsers.propTypes = {
  items: PropTypes.array.isRequired,
  onItemPress: PropTypes.func.isRequired,
};

export default ListUsers;
