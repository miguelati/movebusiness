import colors from './colors';

const fontFamily = {
  notoSansHkBlack: 'NotoSansHK-Black',
  rubik: {
    light: 'Rubik-Light',
    medium: 'Rubik-Medium',
    regular: 'Rubik-Regular',
  },
};

export default {
  notoSans32Black: {
    fontFamily: fontFamily.notoSansHkBlack,
    fontSize: 32,
    color: colors.black,
  },
  notoSans32White: {
    fontFamily: fontFamily.notoSansHkBlack,
    fontSize: 32,
    color: colors.white,
  },
  notoSans32Yellow: {
    fontFamily: fontFamily.notoSansHkBlack,
    fontSize: 32,
    color: colors.yellow,
  },
  rubikRegular26Black: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 26,
    color: colors.black,
  },
  rubikRegular22White: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 22,
    color: colors.white,
  },
  rubikRegular19Black40: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 19,
    color: colors.black40,
  },
  rubikRegular19Black: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 19,
    color: colors.black,
  },
  rubikRegular18White: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 18,
    color: colors.white,
  },
  rubikRegular18Black: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 18,
    color: colors.black,
  },
  rubikRegular18Blue: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 18,
    color: colors.blue,
  },
  rubikRegular16Black: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 16,
    color: colors.black,
  },
  rubikRegular16Yellow: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 16,
    color: colors.yellow,
  },
  rubikRegular14Yellow: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 14,
    color: colors.yellow,
  },
  rubikRegular14Black: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 14,
    color: colors.black,
  },
  rubikRegular12Black: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 12,
    color: colors.black,
  },
  rubikRegular12Yellow: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 12,
    color: colors.yellow,
  },
  rubikRegular12Blue: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 12,
    color: colors.blue,
  },
  rubikRegular9Black: {
    fontFamily: fontFamily.rubik.regular,
    fontSize: 9,
    color: colors.black,
  },
  rubikLight100Black: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 100,
    color: colors.black,
  },
  rubikLight54Black: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 54,
    color: colors.black,
  },
  rubikLight36Black: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 36,
    color: colors.black,
  },
  rubikLight32White: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 32,
    color: colors.white,
  },
  rubikLight32Black: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 32,
    color: colors.black,
  },
  rubikLight22White: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 22,
    color: colors.white,
  },
  rubikLight18Blue: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 18,
    color: colors.blue,
  },
  rubikLight14Blue: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 14,
    color: colors.blue,
  },
  rubikLight14LightGrey: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 14,
    color: colors.lightGrey,
  },
  rubikLight14Black: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 14,
    color: colors.black,
  },
  rubikLight12Black: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 12,
    color: colors.black,
  },
  rubikLight9Black: {
    fontFamily: fontFamily.rubik.light,
    fontSize: 9,
    color: colors.black,
  },
  rubikMedium32Black: {
    fontFamily: fontFamily.rubik.medium,
    fontSize: 32,
    color: colors.black,
  },
  rubikMedium32White: {
    fontFamily: fontFamily.rubik.medium,
    fontSize: 32,
    color: colors.white,
  },
  rubikMedium26White: {
    fontFamily: fontFamily.rubik.medium,
    fontSize: 26,
    color: colors.white,
  },
  rubikMedium18White: {
    fontFamily: fontFamily.rubik.medium,
    fontSize: 18,
    color: colors.white,
  },
  rubikMedium16White: {
    fontFamily: fontFamily.rubik.medium,
    fontSize: 16,
    color: colors.white,
  },
  rubikMedium16Black: {
    fontFamily: fontFamily.rubik.medium,
    fontSize: 16,
    color: colors.black,
  },
  rubikMedium14Blue: {
    fontFamily: fontFamily.rubik.medium,
    fontSize: 14,
    color: colors.blue,
  },
};
