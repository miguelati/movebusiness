import ForgetPassword from './ForgetPassword';
import Home from './Home';
import Login from './Login';
import SignUp from './SignUp';
import Splash from './Splash';
import Wellcome from './Wellcome';
import AuthLoading from './AuthLoading';
import * as Bussines from './Bussines';
import * as Users from './Users';
import * as Menu from './Menu';
import * as Tables from './Tables';
import * as Locals from './Locals';
import * as MenuOptions from './MenuOptions';

export {
  ForgetPassword,
  Home,
  Login,
  SignUp,
  Splash,
  Wellcome,
  AuthLoading,
  MenuOptions,
  Bussines,
  Users,
  Menu,
  Tables,
  Locals,
};
