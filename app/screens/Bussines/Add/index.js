import React from 'react';
import PropTypes from 'prop-types';
import {Keyboard, TouchableOpacity, Image} from 'react-native';
import * as Yup from 'yup';
import {Formik} from 'formik';
import {
  View,
  Container,
  Grid,
  Row,
  Col,
  Button,
  Item,
  Text,
  Label,
  Input,
  Toast,
} from 'native-base';
import {LoadingHUD} from 'react-native-hud-hybrid';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

import {Base, Business, Profile} from '../../../models';
import {NavigationBar} from '../../../components';
import I18n from '../../../i18n';
import styles from './styles';

const validation = Yup.object().shape({
  name: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('businessScreen.add.fields.name.name'),
    }),
  ),
  address: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('businessScreen.add.fields.address.name'),
    }),
  ),
  phone: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('businessScreen.add.fields.phone.name'),
    }),
  ),
});

class BusinessAdd extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      avatarSource: require('../../../commons/img/defaults/business.png'),
      toastPosition: 'bottom',
    };
    this.loadingHUD = new LoadingHUD();
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow.bind(this),
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide.bind(this),
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    this.setState({toastPosition: 'top'});
  }

  _keyboardDidHide() {
    this.setState({toastPosition: 'bottom'});
  }

  onSubmit = async (values, actions) => {
    const {name, address, phone} = values;
    actions.setSubmitting(true);
    this.loadingHUD.show();
    try {
      const userCredential = JSON.parse(
        await AsyncStorage.getItem('userCredential'),
      );
      const business = await Business.add({
        name,
        address,
        phone,
        owner: Profile.getRef(userCredential.user.uid),
      });

      const profile = new Profile(userCredential.user.uid);
      Base.businessId = business;
      await profile.update({
        hasBusiness: true,
        business: Business.getRef(business.id),
      });
      await AsyncStorage.multiSet([
        ['userHasBusiness', JSON.stringify(true)],
        ['business', business.id],
      ]);
      this.loadingHUD.hideAll();
      actions.setSubmitting(false);
      const {navigation} = this.props;
      navigation.navigate('App');
    } catch (error) {
      console.log('error business add', error);
    }
  };

  onRadioButtonChanged = item => {
    console.log(item);
  };

  showError = errors => {
    const {toastPosition} = this.state;
    let errorMessage = Object.keys(errors).map(item => errors[item]);

    if (errorMessage.length > 0) {
      Toast.show({
        text: errorMessage.join('\n'),
        buttonText: '',
        duration: 3000,
        type: 'danger',
        position: toastPosition,
      });
    } else {
      Toast.hide();
    }
  };

  onPictureTap = () => {
    const options = {
      title: I18n.t('users.edit.picture.title'),
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, this.imageProcess);
  };

  imageProcess = async response => {
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else {
      this.profilePictureSmall = await ImageResizer.createResizedImage(
        response.uri,
        300,
        300,
        'JPEG',
        100,
        0,
        null,
      );

      this.profilePictureBig = await ImageResizer.createResizedImage(
        response.uri,
        600,
        600,
        'JPEG',
        100,
        0,
        null,
      );

      const source = {uri: this.profilePictureSmall.uri};

      this.setState({
        avatarSource: source,
      });
    }
  };

  render() {
    const {avatarSource} = this.state;
    return (
      <Container>
        <NavigationBar title="businessScreen.add.navigationBarTitile" />
        <Grid>
          <Row size={2}>
            <Col style={styles.center}>
              <TouchableOpacity
                onPress={this.onPictureTap}
                style={styles.image}>
                <Image style={styles.image} source={avatarSource} />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row size={3}>
            <Col>
              <Formik
                initialValues={{
                  name: '',
                  address: '',
                  phone: '',
                }}
                validationSchema={validation}
                onSubmit={this.onSubmit}>
                {({handleChange, handleBlur, handleSubmit, values, errors}) => {
                  this.showError(errors);
                  return (
                    <View style={styles.formContainer}>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.first_name !== undefined}>
                            <Label>
                              {I18n.t('businessScreen.add.fields.name.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.name}
                              onChangeText={handleChange('name')}
                              onBlur={handleBlur('name')}
                              style={styles.bigInput}
                              placeholder={I18n.t(
                                'businessScreen.add.fields.name.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.last_name !== undefined}>
                            <Label>
                              {I18n.t(
                                'businessScreen.add.fields.address.label',
                              )}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.mainAddress}
                              onChangeText={handleChange('address')}
                              onBlur={handleBlur('address')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'businessScreen.add.fields.address.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.last_name !== undefined}>
                            <Label>
                              {I18n.t('businessScreen.add.fields.phone.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.phoneNumber}
                              onChangeText={handleChange('phone')}
                              onBlur={handleBlur('phone')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'businessScreen.add.fields.phone.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row size={3}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.button}
                            onPress={handleSubmit}>
                            <Text style={styles.buttonText}>
                              {I18n.t('businessScreen.add.save')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                    </View>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}
BusinessAdd.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default BusinessAdd;
