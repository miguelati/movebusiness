import React from 'react';
import * as Yup from 'yup';
import {Keyboard, TouchableOpacity, Image} from 'react-native';
import {Formik} from 'formik';
import {
  View,
  Container,
  Grid,
  Row,
  Col,
  Button,
  Item,
  Text,
  Label,
  Input,
} from 'native-base';
import {LoadingHUD} from 'react-native-hud-hybrid';
import AsyncStorage from '@react-native-community/async-storage';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

import {NavigationBar} from '../../../components';
import {Profile, Business} from '../../../models';
import I18n from '../../../i18n';
import styles from './styles';
import firebase from 'react-native-firebase';

const validation = Yup.object().shape({
  name: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('businessScreen.add.fields.name.name'),
    }),
  ),
  address: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('businessScreen.add.fields.address.name'),
    }),
  ),
  phone: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('businessScreen.add.fields.phone.name'),
    }),
  ),
});

class BussinesUpdate extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      avatarSource: require('../../../commons/img/defaults/business.png'),
      fields: {
        name: '',
        address: '',
        phone: '',
      },
    };
    this.loadingHUD = new LoadingHUD();
    this.getData();
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow.bind(this),
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide.bind(this),
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    this.setState({toastPosition: 'top'});
  }

  _keyboardDidHide() {
    this.setState({toastPosition: 'bottom'});
  }

  getData = async () => {
    this.loadingHUD.show();
    const userCredential = JSON.parse(
      await AsyncStorage.getItem('userCredential'),
    );
    const profile = new Profile(userCredential.user.uid);
    const data = await profile.get();
    const pictureBig = (await data.get('business').get()).data().pictureBig;
    this.setState({
      fields: {...(await data.get('business').get()).data(), id: data.id},
      avatarSource:
        pictureBig != null
          ? {uri: pictureBig}
          : require('../../../commons/img/defaults/business.png'),
    });
    this.loadingHUD.hideAll();
  };

  onSubmit = async (values, actions) => {
    const businessId = (await AsyncStorage.getItem('businessId')).replaceAll(
      '"',
      '',
    );
    actions.setSubmitting(true);
    this.loadingHUD.show();
    try {
      const data = {
        ...values,
      };
      if (this.profilePictureSmall !== undefined) {
        const storageRef = firebase
          .storage()
          .ref(`business/${businessId}/small.jpg`);

        await storageRef.putFile(this.profilePictureSmall.uri);
        data.pictureSmall = await storageRef.getDownloadURL();
      }
      if (this.profilePictureBig !== undefined) {
        const storageRef = firebase
          .storage()
          .ref(`business/${businessId}/big.jpg`);

        await storageRef.putFile(this.profilePictureBig.uri);
        data.pictureBig = await storageRef.getDownloadURL();
      }

      await Business.setMerged(businessId, data);
      // await Business.setMerged(id, name, address, phone);

      this.loadingHUD.hideAll();
      actions.setSubmitting(false);
    } catch (error) {
      console.log('error business add', error);
    }
  };

  onPictureTap = () => {
    const options = {
      title: I18n.t('users.edit.picture.title'),
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, this.imageProcess);
  };

  imageProcess = async response => {
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else {
      this.profilePictureSmall = await ImageResizer.createResizedImage(
        response.uri,
        300,
        300,
        'JPEG',
        100,
        0,
        null,
      );

      this.profilePictureBig = await ImageResizer.createResizedImage(
        response.uri,
        600,
        600,
        'JPEG',
        100,
        0,
        null,
      );

      const source = {uri: this.profilePictureSmall.uri};

      this.setState({
        avatarSource: source,
      });
    }
  };

  render() {
    const {fields, avatarSource} = this.state;
    return (
      <Container>
        <NavigationBar
          showMenu
          title="businessScreen.update.navigationBarTitile"
        />
        <Grid>
          <Row size={2}>
            <Col style={styles.center}>
              <TouchableOpacity
                onPress={this.onPictureTap}
                style={styles.image}>
                <Image style={styles.image} source={avatarSource} />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row size={3}>
            <Col>
              <Formik
                enableReinitialize
                initialValues={fields}
                validationSchema={validation}
                onSubmit={this.onSubmit}>
                {({handleChange, handleBlur, handleSubmit, values, errors}) => {
                  // this.showError(errors);
                  return (
                    <View style={styles.formContainer}>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.name !== undefined}>
                            <Label>
                              {I18n.t(
                                'businessScreen.update.fields.name.label',
                              )}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.name}
                              onChangeText={handleChange('name')}
                              onBlur={handleBlur('name')}
                              style={styles.bigInput}
                              placeholder={I18n.t(
                                'businessScreen.update.fields.name.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.address !== undefined}>
                            <Label>
                              {I18n.t(
                                'businessScreen.update.fields.address.label',
                              )}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.address}
                              onChangeText={handleChange('address')}
                              onBlur={handleBlur('address')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'businessScreen.update.fields.address.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>

                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.phone !== undefined}>
                            <Label>
                              {I18n.t(
                                'businessScreen.update.fields.phone.label',
                              )}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.phone}
                              onChangeText={handleChange('phone')}
                              onBlur={handleBlur('phone')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'businessScreen.update.fields.phone.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>

                      <Row size={3}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.button}
                            onPress={handleSubmit}>
                            <Text style={styles.buttonText}>
                              {I18n.t('loginScreen.accept')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                    </View>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

export default BussinesUpdate;
