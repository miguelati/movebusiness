import React from 'react';
import PropTypes from 'prop-types';
import {Image} from 'react-native';
import {Container, Grid, Row, Col} from 'native-base';
import styles from './styles';

class Wellcome extends React.PureComponent {
  componentDidMount() {
    setTimeout(() => {
      const {navigation} = this.props;
      navigation.push('Auth');
    }, 1000);
  }

  render() {
    return (
      <Container>
        <Grid>
          <Row>
            <Col style={styles.center}>
              <Image
                source={require('../../commons/img/logo/logo-normal.png')}
              />
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

Wellcome.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Wellcome;
