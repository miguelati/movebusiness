import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer: {
    flex: 1,
  },
  title: {
    ...fonts.notoSans32Black,
    marginLeft: 40,
  },
  subTitle: {
    ...fonts.rubikRegular26Black,
    marginLeft: 40,
  },
  item: {
    marginLeft: 40,
    marginRight: 0,
  },
  input: {
    ...fonts.rubikRegular18Black,
  },
  button: {
    justifyContent: 'center',
    marginLeft: 60,
    marginRight: 60,
    marginTop: 40,
    backgroundColor: colors.yellow,
  },
  buttonText: {
    ...fonts.rubikMedium18White,
  },
  buttonLink: {
    justifyContent: 'center',
  },
  link: {
    ...fonts.rubikRegular18Blue,
  },
});
