import React from 'react';
import PropTypes from 'prop-types';
import {Image, Keyboard} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  Container,
  Grid,
  Row,
  Col,
  Text,
  Item,
  Label,
  Input,
  Button,
  View,
  Toast,
} from 'native-base';
import {Formik} from 'formik';
import * as Yup from 'yup';
import firebase from 'react-native-firebase';
import {LoadingHUD} from 'react-native-hud-hybrid';
import {Profile} from '../../models';
import styles from './styles';
import I18n from '../../i18n';

const validation = Yup.object().shape({
  firstName: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('signUpScreen.fields.firstName'),
    }),
  ),
  lastName: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('signUpScreen.fields.lastName'),
    }),
  ),
  email: Yup.string()
    .email(
      I18n.t('validation.invalid', {
        field: I18n.t('signUpScreen.fields.email'),
      }),
    )
    .required(
      I18n.t('validation.required', {
        field: I18n.t('signUpScreen.fields.email'),
      }),
    ),
  password: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('signUpScreen.fields.password'),
    }),
  ),
});

class SingUp extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      toastPosition: 'bottom',
    };
    this.loadingHUD = new LoadingHUD();
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow.bind(this),
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide.bind(this),
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    this.setState({toastPosition: 'top'});
  }

  _keyboardDidHide() {
    this.setState({toastPosition: 'bottom'});
  }

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  onSubmit = async (values, actions) => {
    const {firstName, lastName, email, password} = values;
    actions.setSubmitting(true);
    this.loadingHUD.show();
    try {
      const userCredential = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, password);
      await Profile.setMerged(userCredential.user.uid, {
        firstName,
        lastName,
        hasBusiness: false,
        permission: 'admin',
      });
      await AsyncStorage.setItem(
        'userCredential',
        JSON.stringify(userCredential),
      );
      this.loadingHUD.hideAll();
      actions.setSubmitting(false);
      const {navigation} = this.props;
      navigation.goBack();
    } catch (error) {
      console.log(error);
      this.loadingHUD.hideAll();
      actions.setSubmitting(false);
      let message = I18n.t(`signUpScreen.errors.${error.code}`);

      Toast.show({
        text: message,
        buttonText: '',
        duration: 3000,
        type: 'danger',
      });
    }
  };

  showError = errors => {
    const {toastPosition} = this.state;
    let errorMessage = Object.keys(errors).map(item => errors[item]);

    if (errorMessage.length > 0) {
      Toast.show({
        text: errorMessage.join('\n'),
        buttonText: '',
        duration: 3000,
        type: 'danger',
        position: toastPosition,
      });
    } else {
      Toast.hide();
    }
  };
  render() {
    return (
      <Container>
        <Grid>
          <Row size={3}>
            <Col style={styles.center}>
              <Image
                source={require('../../commons/img/logo/logo-normal.png')}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.title}>{I18n.t('signUpScreen.title')}</Text>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.subTitle}>
                {I18n.t('signUpScreen.subTitle')}
              </Text>
            </Col>
          </Row>
          <Row size={5}>
            <Col>
              <KeyboardAwareScrollView>
                <Formik
                  initialValues={{
                    firstName: '',
                    lastName: '',
                    email: '',
                    password: '',
                  }}
                  validationSchema={validation}
                  onSubmit={this.onSubmit}>
                  {({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    values,
                    errors,
                  }) => {
                    this.showError(errors);
                    return (
                      <View style={styles.formContainer}>
                        <Row>
                          <Col>
                            <Item
                              stackedLabel
                              style={styles.item}
                              error={errors.firstName !== undefined}>
                              <Label>{I18n.t('signUpScreen.firstName')}</Label>
                              <Input
                                autoCapitalize="none"
                                value={values.firstName}
                                onChangeText={handleChange('firstName')}
                                onBlur={handleBlur('firstName')}
                                style={styles.input}
                                placeholder={I18n.t(
                                  'signUpScreen.firstNamePlaceholder',
                                )}
                              />
                            </Item>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Item
                              stackedLabel
                              style={styles.item}
                              error={errors.lastName !== undefined}>
                              <Label>{I18n.t('signUpScreen.lastName')}</Label>
                              <Input
                                autoCapitalize="none"
                                value={values.lastName}
                                onChangeText={handleChange('lastName')}
                                onBlur={handleBlur('lastName')}
                                style={styles.input}
                                placeholder={I18n.t(
                                  'signUpScreen.lastNamePlaceholder',
                                )}
                              />
                            </Item>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Item
                              stackedLabel
                              style={styles.item}
                              error={errors.email !== undefined}>
                              <Label>{I18n.t('signUpScreen.email')}</Label>
                              <Input
                                autoCapitalize="none"
                                value={values.email}
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                                style={styles.input}
                                placeholder={I18n.t(
                                  'signUpScreen.emailPlaceholder',
                                )}
                              />
                            </Item>
                          </Col>
                        </Row>
                        <Row>
                          <Col>
                            <Item
                              stackedLabel
                              style={styles.item}
                              error={errors.password !== undefined}>
                              <Label>{I18n.t('signUpScreen.password')}</Label>
                              <Input
                                value={values.password}
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                style={styles.input}
                                secureTextEntry
                              />
                            </Item>
                          </Col>
                        </Row>
                        <Row size={3}>
                          <Col>
                            <Button
                              rounded
                              warning
                              style={styles.button}
                              onPress={handleSubmit}>
                              <Text style={styles.buttonText}>
                                {I18n.t('signUpScreen.accept')}
                              </Text>
                            </Button>
                            <Button
                              transparent
                              style={styles.buttonLink}
                              onPress={this.goBack}>
                              <Text style={styles.link}>
                                {I18n.t('signUpScreen.back')}
                              </Text>
                            </Button>
                          </Col>
                        </Row>
                      </View>
                    );
                  }}
                </Formik>
              </KeyboardAwareScrollView>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

SingUp.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default SingUp;
