import React from 'react';
import * as Yup from 'yup';
import {Formik} from 'formik';
import {
  View,
  Container,
  Grid,
  Row,
  Col,
  Button,
  Item,
  Text,
  Label,
  Input,
  Switch,
} from 'native-base';
import HUD, {LoadingHUD} from 'react-native-hud-hybrid';
import {NavigationBar} from '../../../components';
import {Table} from '../../../models';
import I18n from '../../../i18n';
import styles from './styles';

const validation = Yup.object().shape({
  tableNumber: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('tables.add.fields.tableNumber.name'),
    }),
  ),
  totalChairs: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('tables.add.fields.totalChairs.name'),
    }),
  ),
});

class UsersAdd extends React.PureComponent {
  constructor(props) {
    super(props);
    const {localId} = this.props.navigation.state.params;

    this.state = {
      localId,
      table: {
        tableNumber: '',
        totalChairs: '',
      },
      smokingArea: false,
    };
    this.loadingHUD = new LoadingHUD();
  }
  onSubmit = async values => {
    try {
      const {localId, smokingArea} = this.state;
      this.loadingHUD.show();
      const data = {...values, smokingArea, available: true};
      await Table.add(localId, data);
      this.loadingHUD.hideAll();
      const {navigation} = this.props;
      navigation.goBack();
    } catch (error) {
      console.log(error);
    }
  };

  onRadioButtonChanged = item => {
    console.log(item);
  };

  render() {
    const {smokingArea} = this.state;
    return (
      <Container>
        <NavigationBar
          back
          barStyle="yellow"
          title="tables.add.navigationBarTitile"
        />
        <Grid>
          <Row size={3}>
            <Col>
              <Formik
                initialValues={{
                  tableNumber: '',
                  totalChairs: '',
                  smokingArea: false,
                }}
                validationSchema={validation}
                onSubmit={this.onSubmit}>
                {({handleChange, handleBlur, handleSubmit, values, errors}) => {
                  // this.showError(errors);
                  return (
                    <View style={styles.formContainer}>
                      <Row size={2} />
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.first_name !== undefined}>
                            <Label>
                              {I18n.t('tables.add.fields.tableNumber.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.first_name}
                              onChangeText={handleChange('tableNumber')}
                              onBlur={handleBlur('tableNumber')}
                              style={styles.bigInput}
                              placeholder={I18n.t(
                                'tables.add.fields.tableNumber.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.last_name !== undefined}>
                            <Label>
                              {I18n.t('tables.add.fields.totalChairs.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.last_name}
                              onChangeText={handleChange('totalChairs')}
                              onBlur={handleBlur('totalChairs')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'tables.add.fields.totalChairs.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.itemUnderlined}
                            error={errors.last_name !== undefined}>
                            <Label>
                              {I18n.t('tables.add.fields.smokingArea.label')}:
                            </Label>
                            <Switch
                              style={styles.switch}
                              trackColor={{
                                false: '#767577',
                                true: 'light-green',
                              }}
                              onValueChange={() => {
                                this.setState(prevState => ({
                                  smokingArea: !prevState.smokingArea,
                                }));
                              }}
                              value={smokingArea}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row size={1}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.button}
                            onPress={handleSubmit}>
                            <Text style={styles.buttonText}>
                              {I18n.t('loginScreen.accept')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                    </View>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

export default UsersAdd;
