import React from 'react';
import PropTypes from 'prop-types';
import {Image, TouchableOpacity} from 'react-native';
import {Container, Grid, Row, Col, Text} from 'native-base';
import {
  NavigationBar,
  SearchBoxBigger,
  ListTables,
  TitleScroll,
} from '../../../components';
import {Local, Table} from '../../../models';
import I18n from '../../../i18n';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';

class TablesList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      locals: [],
      tables: [],
    };

    this.getLocals();
  }

  getLocals = async () => {
    const businessId = (await AsyncStorage.getItem('businessId')).replaceAll(
      '"',
      '',
    );
    this.localsSnap = Local.onSnapshot(businessId, snapshot => {
      if (snapshot.docs.length > 0) {
        this.setState(
          {
            locals: snapshot.docs,
          },
          () => this.getTables(snapshot.docs[0].id),
        );
      }
    });
  };

  getTables = localId => {
    this.selected = localId;
    if (this.tablesSnap) {
      this.tablesSnap();
    }
    this.tablesSnap = Table.onSnapshot(localId, snap => {
      this.setState({tables: snap.docs});
    });
  };

  componentWillUnmount() {
    if (this.localsSnap) {
      this.localsSnap();
    }
    if (this.tablesSnap) {
      this.tablesSnap();
    }
  }

  addButtonHandler = () => {
    const {navigation} = this.props;
    navigation.navigate('TablesAdd', {localId: this.selected});
  };

  searchBoxChange = text => {
    console.log(text);
  };

  onItemPress = item => {
    console.log(item);
  };

  onTitleScrollChanged = item => {
    const {locals} = this.state;
    const selected = locals.filter(local => local.get('name') === item)[0];
    this.getTables(selected.id);
  };

  render() {
    const {locals, tables} = this.state;
    return (
      <Container>
        <NavigationBar showMenu withTitle={false} />
        <Grid>
          <Row style={[styles.header, styles.center]}>
            <Col style={styles.titleContainer}>
              <Text style={styles.title}>{I18n.t('tables.list.title')}</Text>
            </Col>
            <Col style={styles.buttonAddContainer}>
              <TouchableOpacity onPress={this.addButtonHandler}>
                <Image
                  style={styles.buttonAdd}
                  source={require('../../../commons/img/buttons/add.png')}
                />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={[styles.header, styles.center]}>
            <Col>
              <SearchBoxBigger
                placeholder={I18n.t('tables.list.searchPlaceholder')}
                onChange={this.searchBoxChange}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <TitleScroll
                items={locals.map(item => item.get('name'))}
                onChange={this.onTitleScrollChanged}
              />
            </Col>
          </Row>
          <Row size={10}>
            <Col>
              <ListTables items={tables} onItemPress={this.onItemPress} />
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

TablesList.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default TablesList;
