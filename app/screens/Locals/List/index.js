import React from 'react';
import PropTypes from 'prop-types';
import {Image, TouchableOpacity} from 'react-native';
import {Container, Grid, Row, Col, Text} from 'native-base';
import {NavigationBar, SearchBoxBigger, ListLocals} from '../../../components';
import {Local} from '../../../models';
import I18n from '../../../i18n';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';

class LocalsList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      locals: [],
    };
  }

  async componentDidMount() {
    const businessId = (await AsyncStorage.getItem('businessId')).replaceAll(
      '"',
      '',
    );
    this.unsuscribe = Local.onSnapshot(businessId, snapshot => {
      const locals = this.processData(snapshot.docs);
      this.setState({locals, localsOriginal: locals});
    });
  }

  componentWillUnmount() {
    this.unsuscribe();
  }

  processData = locals => {
    return locals.map(item => ({
      id: item.id,
      name: item.get('name'),
      address: item.get('address'),
      source: item,
    }));
  };

  addButtonHandler = () => {
    const {navigation} = this.props;
    navigation.navigate('LocalAdd');
  };

  searchBoxChange = text => {
    if (text === '') {
      this.setState(prevState => ({locals: prevState.localsOriginal}));
    } else {
      this.setState(prevState => {
        return {
          locals: prevState.localsOriginal.filter(
            item =>
              item.name.indexOf(text) !== -1 ||
              item.address.indexOf(text) !== -1,
          ),
        };
      });
    }
  };

  onItemPress = item => {
    const {navigation} = this.props;
    navigation.navigate('LocalEdit', {local: item.source});
  };

  render() {
    const {locals} = this.state;
    return (
      <Container>
        <NavigationBar showMenu withTitle={false} />
        <Grid>
          <Row style={[styles.header, styles.center]}>
            <Col style={styles.titleContainer}>
              <Text style={styles.title}>
                {I18n.t('localsScreen.list.title')}
              </Text>
            </Col>
            <Col style={styles.buttonAddContainer}>
              <TouchableOpacity onPress={this.addButtonHandler}>
                <Image
                  style={styles.buttonAdd}
                  source={require('../../../commons/img/buttons/add.png')}
                />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={[styles.header, styles.center]}>
            <Col>
              <SearchBoxBigger
                placeholder={I18n.t('localsScreen.list.searchPlaceholder')}
                onChange={this.searchBoxChange}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <ListLocals items={locals} onItemPress={this.onItemPress} />
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

LocalsList.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default LocalsList;
