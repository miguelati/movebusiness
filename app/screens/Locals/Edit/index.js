import React from 'react';
import PropTypes from 'prop-types';
import {Alert} from 'react-native';
import * as Yup from 'yup';
import {Formik} from 'formik';
import {View, Container, Grid, Row, Col, Button, Text} from 'native-base';
import HUD, {LoadingHUD} from 'react-native-hud-hybrid';
import {Local} from '../../../models';
import {NavigationBar, InputCustom} from '../../../components';
import I18n from '../../../i18n';
import styles from './styles';

const validation = Yup.object().shape({
  name: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.add.fields.name.name'),
    }),
  ),
  address: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.add.fields.address.name'),
    }),
  ),
  phone: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.add.fields.phone.name'),
    }),
  ),
});

class LocalEdit extends React.PureComponent {
  constructor(props) {
    super(props);
    const {navigation} = this.props;
    this.local = navigation.state.params.local;
    this.state = {
      local: {
        name: this.local.get('name'),
        address: this.local.get('address'),
        phone: this.local.get('phone'),
      },
    };
    this.loadingHUD = new LoadingHUD();
  }

  componentDidMount() {
    HUD.config({
      loadingText: 'cargando...',
    });
  }

  onRemove = async () => {
    Alert.alert(
      I18n.t('localsScreen.edit.removeMessage.title'),
      I18n.t('localsScreen.edit.removeMessage.message'),
      [
        {
          text: I18n.t('localsScreen.edit.removeMessage.yes'),
          onPress: async () => {
            this.loadingHUD.show();
            await Local.remove(this.local.id);
            this.loadingHUD.hideAll();
            const {navigation} = this.props;
            navigation.goBack();
          },
        },
        {
          text: I18n.t('localsScreen.edit.removeMessage.no'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  };

  onSubmit = async values => {
    try {
      this.loadingHUD.show();
      await Local.setMerged(this.local.id, values);
      this.loadingHUD.hideAll();
      const {navigation} = this.props;
      navigation.goBack();
    } catch (error) {
      console.log(error);
    }
  };

  render() {
    const {local} = this.state;
    return (
      <Container>
        <NavigationBar back title="localsScreen.edit.navigationBarTitile" />
        <Grid>
          <Row size={1}>
            <Col style={styles.center} />
          </Row>
          <Row size={3}>
            <Col>
              <Formik
                initialValues={local}
                validationSchema={validation}
                onSubmit={this.onSubmit}>
                {({handleChange, handleBlur, handleSubmit, values, errors}) => {
                  // this.showError(errors);
                  return (
                    <View style={styles.formContainer}>
                      <Row>
                        <Col>
                          <InputCustom
                            label={I18n.t(
                              'localsScreen.edit.fields.name.label',
                            )}
                            placeholder={I18n.t(
                              'localsScreen.edit.fields.name.placeholder',
                            )}
                            value={values.name}
                            error={errors.name}
                            handleChange={handleChange('name')}
                            handleBlur={handleBlur('name')}
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <InputCustom
                            label={I18n.t(
                              'localsScreen.edit.fields.address.label',
                            )}
                            placeholder={I18n.t(
                              'localsScreen.edit.fields.address.placeholder',
                            )}
                            value={values.address}
                            error={errors.address}
                            handleChange={handleChange('address')}
                            handleBlur={handleBlur('address')}
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <InputCustom
                            label={I18n.t(
                              'localsScreen.edit.fields.phone.label',
                            )}
                            placeholder={I18n.t(
                              'localsScreen.edit.fields.phone.placeholder',
                            )}
                            value={values.phone}
                            error={errors.phone}
                            handleChange={handleChange('phone')}
                            handleBlur={handleBlur('phone')}
                          />
                        </Col>
                      </Row>
                      <Row size={1}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.button}
                            onPress={handleSubmit}>
                            <Text style={styles.buttonText}>
                              {I18n.t('localsScreen.edit.save')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row size={1}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.buttonRemove}
                            onPress={this.onRemove}>
                            <Text style={styles.buttonText}>
                              {I18n.t('users.edit.remove')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row size={2} />
                    </View>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

LocalEdit.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default LocalEdit;
