import React from 'react';
import {View, Text} from 'react-native';
import PropTypes from 'prop-types';

class Home extends React.PureComponent {
  render() {
    return (
      <View>
        <Text>Home</Text>
      </View>
    );
  }
}

export default Home;
