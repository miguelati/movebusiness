import React from 'react';
import PropTypes from 'prop-types';
import {Image, TouchableOpacity, Alert} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import * as Yup from 'yup';
import {Formik} from 'formik';
import {
  View,
  Container,
  Grid,
  Row,
  Col,
  Button,
  Item,
  Text,
  Label,
  Input,
  Picker,
} from 'native-base';
import HUD, {LoadingHUD} from 'react-native-hud-hybrid';
import firebase from 'react-native-firebase';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import {Profile, Business, Local} from '../../../models';
import {NavigationBar, RadioGroups} from '../../../components';
import I18n from '../../../i18n';
import styles from './styles';
import {colors} from '../../../themes';

const validation = Yup.object().shape({
  firstName: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.edit.fields.first_name.name'),
    }),
  ),
  lastName: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.edit.fields.last_name.name'),
    }),
  ),
  email: Yup.string()
    .email(
      I18n.t('validation.invalid', {
        field: I18n.t('users.edit.fields.email.name'),
      }),
    )
    .required(
      I18n.t('validation.required', {
        field: I18n.t('users.edit.fields.email.name'),
      }),
    ),
  cellphone: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.edit.fields.cellphone.name'),
    }),
  ),
  permission: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.edit.fields.permission.name'),
    }),
  ),
});

class UsersEdit extends React.PureComponent {
  constructor(props) {
    super(props);
    const {navigation} = this.props;
    this.user = navigation.state.params.user.source;
    this.state = {
      avatarSource: this.user.get('pictureSmall')
        ? {uri: this.user.get('pictureSmall')}
        : require('../../../commons/img/defaults/user.png'),
      locals: [],
      user: {
        firstName: this.user.get('firstName'),
        lastName: this.user.get('lastName'),
        email: this.user.get('email'),
        cellphone: this.user.get('cellphone'),
        permission: this.user.get('permission'),
        pictureSmall: this.user.get('pictureSmall'),
      },
    };
    this.loadingHUD = new LoadingHUD();
  }

  componentDidMount() {
    HUD.config({
      loadingText: 'cargando...',
    });

    this.getLocals();
  }

  getLocals = async () => {
    const businessId = (await AsyncStorage.getItem('businessId')).replaceAll(
      '"',
      '',
    );
    this.localsSnap = Local.onSnapshot(businessId, snapshot => {
      // Process locals snapshot
      const locals = this.processLocals(snapshot.docs);
      console.log(locals);
      this.setState({locals});
    });
  };

  processLocals = docs => {
    return docs.map(doc => ({
      id: doc.id,
      name: doc.get('name'),
      ref: doc.ref,
    }));
  };

  onSubmit = async values => {
    try {
      this.loadingHUD.show();
      const selectedLocal = this.state.locals.find(
        local => local.id === values.localId,
      );
      const data = {
        ...values,
        local: selectedLocal.ref,
      };
      delete data.localId;

      if (this.profilePictureSmall !== undefined) {
        const storageRef = firebase
          .storage()
          .ref(`profile/${this.user.id}/small.jpg`);

        await storageRef.putFile(this.profilePictureSmall.uri);
        data.pictureSmall = await storageRef.getDownloadURL();
      }
      if (this.profilePictureBig !== undefined) {
        const storageRef = firebase
          .storage()
          .ref(`profile/${this.user.id}/big.jpg`);

        await storageRef.putFile(this.profilePictureBig.uri);
        data.pictureBig = await storageRef.getDownloadURL();
      }
      await Profile.setMerged(this.user.id, data);
      this.loadingHUD.hideAll();
      const {navigation} = this.props;
      navigation.goBack();
    } catch (error) {
      console.log(error);
    }
  };

  onRemove = async () => {
    Alert.alert(
      I18n.t('users.edit.removeMessage.title'),
      I18n.t('users.edit.removeMessage.message'),
      [
        {
          text: I18n.t('users.edit.removeMessage.yes'),
          onPress: async () => {
            this.loadingHUD.show();
            await Profile.remove(this.user.id);
            if (this.profilePictureSmall !== undefined) {
              const storageRef = firebase
                .storage()
                .ref(`profile/${this.user.id}/small.jpg`);

              await storageRef.delete();
            }
            if (this.profilePictureBig !== undefined) {
              const storageRef = firebase
                .storage()
                .ref(`profile/${this.user.id}/big.jpg`);

              await storageRef.delete();
            }
            this.loadingHUD.hideAll();
            const {navigation} = this.props;
            navigation.goBack();
          },
        },
        {
          text: I18n.t('users.edit.removeMessage.no'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  };

  onPictureTap = () => {
    const options = {
      title: I18n.t('users.edit.picture.title'),
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, this.imageProcess);
  };

  imageProcess = async response => {
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else {
      this.profilePictureSmall = await ImageResizer.createResizedImage(
        response.uri,
        300,
        300,
        'JPEG',
        100,
        0,
        null,
      );

      this.profilePictureBig = await ImageResizer.createResizedImage(
        response.uri,
        600,
        600,
        'JPEG',
        100,
        0,
        null,
      );

      const source = {uri: this.profilePictureSmall.uri};

      this.setState({
        avatarSource: source,
      });
    }
  };

  render() {
    const {avatarSource, user, locals} = this.state;

    return (
      <Container>
        <NavigationBar back title="users.edit.navigationBarTitile" />
        <Grid>
          <Row size={1}>
            <Col style={styles.center}>
              <TouchableOpacity onPress={this.onPictureTap}>
                <Image style={styles.image} source={avatarSource} />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row size={3}>
            <Col>
              <Formik
                initialValues={{...user, localId: this.user.get('local').id}}
                validationSchema={validation}
                onSubmit={this.onSubmit}>
                {({
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  values,
                  errors,
                  setFieldValue,
                }) => {
                  // this.showError(errors);
                  return (
                    <View style={styles.formContainer}>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.firstName !== undefined}>
                            <Label>
                              {I18n.t('users.edit.fields.firstName.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.firstName}
                              onChangeText={handleChange('firstName')}
                              onBlur={handleBlur('firstName')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'users.edit.fields.firstName.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.lastName !== undefined}>
                            <Label>
                              {I18n.t('users.edit.fields.lastName.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.lastName}
                              onChangeText={handleChange('lastName')}
                              onBlur={handleBlur('lastName')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'users.edit.fields.lastName.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.email !== undefined}>
                            <Label>
                              {I18n.t('users.edit.fields.email.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.email}
                              onChangeText={handleChange('email')}
                              onBlur={handleBlur('email')}
                              editable={false}
                              style={styles.input}
                              placeholder={I18n.t(
                                'users.edit.fields.email.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.cellphone !== undefined}>
                            <Label>
                              {I18n.t('users.edit.fields.cellphone.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.cellphone}
                              onChangeText={handleChange('cellphone')}
                              onBlur={handleBlur('cellphone')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'users.edit.fields.cellphone.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.localId !== undefined}>
                            <Label>
                              {I18n.t('users.add.fields.local.label')}
                            </Label>
                            <Picker
                              note
                              mode="modal"
                              value={values.localId}
                              placeholder={I18n.t(
                                'users.add.fields.local.placeholder',
                              )}
                              placeholderStyle={{color: '#565656'}}
                              style={styles.picker}
                              textStyle={{color: colors.black}}
                              selectedValue={values.localId}
                              onValueChange={handleChange('localId')}
                              onBlur={handleBlur('localId')}>
                              {locals.map(local => (
                                <Picker.Item
                                  label={local.name}
                                  value={local.id}
                                  key={local.id}
                                />
                              ))}
                            </Picker>
                          </Item>
                        </Col>
                      </Row>
                      <Row size={2}>
                        <Col>
                          <Row size={1}>
                            <Col>
                              <Label style={styles.label}>
                                {I18n.t('users.edit.fields.permission.label')}
                              </Label>
                            </Col>
                          </Row>
                          <Row size={4}>
                            <Col>
                              <RadioGroups
                                items={['admin', 'waiter', 'chef']}
                                selected={values.permission}
                                style={styles.radio}
                                onChanged={value =>
                                  setFieldValue('permission', value)
                                }
                              />
                            </Col>
                          </Row>
                          <Row style={styles.line}>
                            <Col />
                          </Row>
                          <View />
                        </Col>
                      </Row>
                      <Row size={1}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.button}
                            onPress={handleSubmit}>
                            <Text style={styles.buttonText}>
                              {I18n.t('loginScreen.accept')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row size={1}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.buttonRemove}
                            onPress={this.onRemove}>
                            <Text style={styles.buttonText}>
                              {I18n.t('users.edit.remove')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                    </View>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

UsersEdit.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default UsersEdit;
