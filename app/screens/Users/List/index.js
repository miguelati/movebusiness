import React from 'react';
import PropTypes from 'prop-types';
import AsyncStorage from '@react-native-community/async-storage';
import {ActionSheetIOS, Image, TouchableOpacity} from 'react-native';
import {Container, Grid, Row, Col, Text} from 'native-base';
import {Profile} from '../../../models';
import {NavigationBar, SearchBoxBigger, ListUsers} from '../../../components';
import I18n from '../../../i18n';
import styles from './styles';

class UsersList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      users: [],
      usersOriginal: [],
    };

    AsyncStorage.getItem('businessId').then(businessId => {
      this.usersDisconnect = Profile.getByBusiness(
        JSON.parse(businessId),
        snapshot => {
          console.log('Usuarios:', snapshot.docs.length);
          const users = this.processData(snapshot.docs);
          this.setState({users, usersOriginal: users});
        },
      );
    });
  }

  componentWillUnmount() {
    if (this.usersDisconnect !== undefined) {
      this.usersDisconnect();
    }
  }

  processData = users => {
    return users
      .map(item => ({
        id: item.id,
        first_name: item.get('firstName'),
        last_name: item.get('lastName'),
        permission: item.get('permission'),
        pictureSmall: item.get('pictureSmall'),
        source: item,
      }))
      .filter(item => item.id !== Profile.currentUserId);
  };

  addButtonHandler = () => {
    const {navigation} = this.props;
    navigation.navigate('UsersAdd');
  };

  searchBoxChange = text => {
    if (text === '') {
      this.setState(prevState => ({users: prevState.usersOriginal}));
    } else {
      this.setState(prevState => {
        return {
          users: prevState.usersOriginal.filter(
            item =>
              item.first_name.indexOf(text) !== -1 ||
              item.last_name.indexOf(text) !== -1 ||
              I18n.t(`users.list.permissions.${item.permission}`).indexOf(
                text,
              ) !== -1,
          ),
        };
      });
    }
  };

  onItemPress = item => {
    const {navigation} = this.props;
    navigation.navigate('UserEdit', {user: item});

    /* ActionSheetIOS.showActionSheetWithOptions(
      {
        options: ['Cancel', 'Edit user', 'View history'],
        cancelButtonIndex: 0,
      },
      buttonIndex => {
        if (buttonIndex === 1) {
          this.goToEditUser(item);
        } else if (buttonIndex === 2) {
          this.goToUserHistory(item);
        }
      },
    ); */
  };

  goToEditUser = item => {
    const {navigation} = this.props;
    navigation.navigate('UserEdit', {user: item});
  };

  goToUserHistory = item => {
    const {navigation} = this.props;
    navigation.navigate('UserHistory', {user: item});
  };

  render() {
    const {users} = this.state;
    return (
      <Container>
        <NavigationBar showMenu withTitle={false} />
        <Grid>
          <Row style={[styles.header, styles.center]}>
            <Col style={styles.titleContainer}>
              <Text style={styles.title}>{I18n.t('users.list.title')}</Text>
            </Col>
            <Col style={styles.buttonAddContainer}>
              <TouchableOpacity onPress={this.addButtonHandler}>
                <Image
                  style={styles.buttonAdd}
                  source={require('../../../commons/img/buttons/add.png')}
                />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={[styles.header, styles.center]}>
            <Col>
              <SearchBoxBigger
                placeholder={I18n.t('users.list.searchPlaceholder')}
                onChange={this.searchBoxChange}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <ListUsers items={users} onItemPress={this.onItemPress} />
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

UsersList.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default UsersList;
