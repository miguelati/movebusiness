import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../../themes';

export default EStyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  formContainer: {
    flex: 1,
    marginLeft: 20,
  },
  image: {
    width: 190,
    height: 190,
    borderRadius: 95,
  },
  header: {
    backgroundColor: colors.lightWhite,
    height: 60,
  },
  title: {
    ...fonts.rubikMedium32Black,
    marginLeft: 18,
  },
  item: {
    alignItems: 'flex-start',
  },
  label: {
    ...fonts.rubikRegular14Yellow,
  },
  radio: {
    marginTop: 10,
  },
  picker: {
    height: 40,
    marginLeft: -10,
  },
  line: {
    height: 1,
    backgroundColor: colors.minorGrey,
  },
  titleContainer: {},
  buttonAddContainer: {
    alignItems: 'flex-end',
  },
  button: {
    justifyContent: 'center',
    marginLeft: 60,
    marginRight: 60,
    marginTop: 10,
    backgroundColor: colors.yellow,
  },
  buttonText: {
    ...fonts.rubikMedium18White,
  },
});
