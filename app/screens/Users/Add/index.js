import React from 'react';
import PropTypes from 'prop-types';
import {Image, TouchableOpacity} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import * as Yup from 'yup';
import {Formik} from 'formik';
import {
  View,
  Container,
  Grid,
  Row,
  Col,
  Button,
  Item,
  Text,
  Label,
  Input,
  Picker,
} from 'native-base';
import HUD, {LoadingHUD} from 'react-native-hud-hybrid';
import firebase from 'react-native-firebase';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import {Profile, Business, Local} from '../../../models';
import {NavigationBar, RadioGroups} from '../../../components';
import I18n from '../../../i18n';
import styles from './styles';
import {colors} from '../../../themes';

const validation = Yup.object().shape({
  firstName: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.add.fields.first_name.name'),
    }),
  ),
  lastName: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.add.fields.last_name.name'),
    }),
  ),
  email: Yup.string()
    .email(
      I18n.t('validation.invalid', {
        field: I18n.t('users.add.fields.email.name'),
      }),
    )
    .required(
      I18n.t('validation.required', {
        field: I18n.t('users.add.fields.email.name'),
      }),
    ),
  cellphone: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.add.fields.cellphone.name'),
    }),
  ),
  // Add validation to the local selection
  // local: Yup.string().required()
  permission: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('users.add.fields.permission.name'),
    }),
  ),
});

class UsersAdd extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      avatarSource: require('../../../commons/img/defaults/user.png'),
      locals: [],
    };
    this.loadingHUD = new LoadingHUD();
  }

  componentDidMount() {
    HUD.config({
      loadingText: 'Cargando...',
    });

    this.getLocals();
  }

  componentWillUnmount() {
    if (this.localsSnap) {
      this.localsSnap();
    }
  }

  getLocals = async () => {
    const businessId = (await AsyncStorage.getItem('businessId')).replaceAll(
      '"',
      '',
    );
    this.localsSnap = Local.onSnapshot(businessId, snapshot => {
      // Process locals snapshot
      const locals = this.processLocals(snapshot.docs);
      this.setState({locals});
    });
  };

  processLocals = docs => {
    return docs.map(doc => ({
      id: doc.id,
      name: doc.get('name'),
    }));
  };

  onSubmit = async values => {
    try {
      const {email} = values;
      this.loadingHUD.show();
      const credential = await firebase
        .auth()
        .createUserWithEmailAndPassword(email, 'abc12345@');
      await firebase.auth().sendPasswordResetEmail(email);
      const businessId = await AsyncStorage.getItem('businessId');
      const selectedLocal = this.state.locals.find(
        local => local.id === values.localId,
      );
      const data = {
        ...values,
        business: Business.getRef(businessId.replaceAll('"', '')),
        local: selectedLocal.ref,
      };
      delete data.localId;

      if (this.profilePictureSmall !== undefined) {
        const storageRef = firebase
          .storage()
          .ref(`profile/${credential.user.uid}/small.jpg`);

        await storageRef.putFile(this.profilePictureSmall.uri);
        data.pictureSmall = await storageRef.getDownloadURL();
      }
      if (this.profilePictureBig !== undefined) {
        const storageRef = firebase
          .storage()
          .ref(`profile/${credential.user.uid}/big.jpg`);

        await storageRef.putFile(this.profilePictureBig.uri);
        data.pictureBig = await storageRef.getDownloadURL();
      }

      await Profile.setMerged(credential.user.uid, data);
      this.loadingHUD.hideAll();
      const {navigation} = this.props;
      navigation.goBack();
    } catch (error) {
      console.log(error);
    }
  };

  onPictureTap = () => {
    const options = {
      title: I18n.t('users.add.picture.title'),
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, this.imageProcess);
  };

  imageProcess = async response => {
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else {
      this.profilePictureSmall = await ImageResizer.createResizedImage(
        response.uri,
        300,
        300,
        'JPEG',
        100,
        0,
        null,
      );

      this.profilePictureBig = await ImageResizer.createResizedImage(
        response.uri,
        600,
        600,
        'JPEG',
        100,
        0,
        null,
      );

      const source = {uri: this.profilePictureSmall.uri};

      this.setState({
        avatarSource: source,
      });
    }
  };

  render() {
    const {avatarSource, locals} = this.state;

    return (
      <Container>
        <NavigationBar back title="users.add.navigationBarTitile" />
        <Grid>
          <Row size={1}>
            <Col style={styles.center}>
              <TouchableOpacity onPress={this.onPictureTap}>
                <Image style={styles.image} source={avatarSource} />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row size={3}>
            <Col>
              <Formik
                initialValues={{
                  firstName: '',
                  lastName: '',
                  email: '',
                  cellphone: '',
                  local: '',
                  permission: '',
                }}
                validationSchema={validation}
                onSubmit={this.onSubmit}>
                {({
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  values,
                  errors,
                  setFieldValue,
                }) => {
                  // this.showError(errors);
                  return (
                    <View style={styles.formContainer}>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.firstName !== undefined}>
                            <Label>
                              {I18n.t('users.add.fields.firstName.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.firstName}
                              onChangeText={handleChange('firstName')}
                              onBlur={handleBlur('firstName')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'users.add.fields.firstName.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.lastName !== undefined}>
                            <Label>
                              {I18n.t('users.add.fields.lastName.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.lastName}
                              onChangeText={handleChange('lastName')}
                              onBlur={handleBlur('lastName')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'users.add.fields.lastName.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.email !== undefined}>
                            <Label>
                              {I18n.t('users.add.fields.email.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.email}
                              onChangeText={handleChange('email')}
                              onBlur={handleBlur('email')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'users.add.fields.email.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.cellphone !== undefined}>
                            <Label>
                              {I18n.t('users.add.fields.cellphone.label')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.cellphone}
                              onChangeText={handleChange('cellphone')}
                              onBlur={handleBlur('cellphone')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'users.add.fields.cellphone.placeholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.localId !== undefined}>
                            <Label>
                              {I18n.t('users.add.fields.local.label')}
                            </Label>
                            <Picker
                              note
                              mode="modal"
                              placeholder={I18n.t(
                                'users.add.fields.local.placeholder',
                              )}
                              placeholderStyle={{color: '#565656'}}
                              style={styles.picker}
                              textStyle={{color: colors.black}}
                              selectedValue={values.localId}
                              onValueChange={handleChange('localId')}
                              onBlur={handleBlur('localId')}>
                              {locals.map(local => (
                                <Picker.Item
                                  label={local.name}
                                  value={local.id}
                                  key={local.id}
                                />
                              ))}
                            </Picker>
                          </Item>
                        </Col>
                      </Row>
                      <Row size={2}>
                        <Col>
                          <Row size={1}>
                            <Col>
                              <Label style={styles.label}>
                                {I18n.t('users.add.fields.permission.label')}
                              </Label>
                            </Col>
                          </Row>
                          <Row size={4}>
                            <Col>
                              <RadioGroups
                                items={['admin', 'waiter', 'chef']}
                                style={styles.radio}
                                onChanged={value =>
                                  setFieldValue('permission', value)
                                }
                              />
                            </Col>
                          </Row>
                          <Row style={styles.line}>
                            <Col />
                          </Row>
                          <View />
                        </Col>
                      </Row>
                      <Row size={1}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.button}
                            onPress={handleSubmit}>
                            <Text style={styles.buttonText}>
                              {I18n.t('loginScreen.accept')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                    </View>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

UsersAdd.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default UsersAdd;
