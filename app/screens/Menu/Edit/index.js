import React from 'react';
import PropTypes from 'prop-types';
import {Image, TouchableOpacity} from 'react-native';
import * as Yup from 'yup';
import {Formik} from 'formik';
import {View, Container, Grid, Row, Col, Button, Text} from 'native-base';
import HUD, {LoadingHUD} from 'react-native-hud-hybrid';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';
import {snakeCase} from 'lodash';

import {Menu} from '../../../models';
import {NavigationBar, InputCustom} from '../../../components';
import I18n from '../../../i18n';
import styles from './styles';
import firebase from 'react-native-firebase';

const validation = Yup.object().shape({
  name: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('menu.add.fields.name.name'),
    }),
  ),
  description: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('menu.add.fields.description.name'),
    }),
  ),
  price: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('menu.add.fields.price.name'),
    }),
  ),
});

class MenuEdit extends React.PureComponent {
  constructor(props) {
    super(props);
    this.menu = this.props.navigation.state.params.menu;
    this.state = {
      editing: false,
      menuOption: this.props.navigation.state.params.menuOption,
      avatarSource: this.menu.pictureBig
        ? {uri: this.menu.pictureBig}
        : require('../../../commons/img/defaults/food.png'),
    };
    this.loadingHUD = new LoadingHUD();
  }

  componentDidMount() {
    HUD.config({
      loadingText: 'cargando...',
    });
  }

  menuIsEdited = values => {
    const hasPicture =
      this.menuPictureSmall !== undefined && this.menuPictureBig !== undefined;
    const nameIsEdited = this.menu.product !== values.name;
    const descriptionIsEdited = this.menu.description !== values.description;
    const priceIsEdited = this.menu.price !== values.price;

    return hasPicture || nameIsEdited || descriptionIsEdited || priceIsEdited;
  };

  onSubmit = async values => {
    const {menuOption} = this.state;
    const {navigation} = this.props;
    this.loadingHUD.show();

    if (!this.menuIsEdited(values)) {
      this.loadingHUD.hideAll();
      navigation.popToTop();
    }
    try {
      const data = {...values};

      if (this.menuPictureSmall !== undefined) {
        const storageRef = firebase
          .storage()
          .ref(
            `menuOptions/${menuOption.id}/menu/${snakeCase(
              values.name,
            )}_small.jpg`,
          );

        await storageRef.putFile(this.menuPictureSmall.uri);
        data.pictureSmall = await storageRef.getDownloadURL();
      }
      if (this.menuPictureBig !== undefined) {
        const storageRef = firebase
          .storage()
          .ref(
            `menuOptions/${menuOption.id}/menu/${snakeCase(
              values.name,
            )}_big.jpg`,
          );

        await storageRef.putFile(this.menuPictureBig.uri);
        data.pictureBig = await storageRef.getDownloadURL();
      }

      // await Menu.add(menuOption.id, data);
      await Menu.setMerged(menuOption.id, this.menu.id, data);
      this.loadingHUD.hideAll();

      navigation.popToTop();
    } catch (error) {
      console.log(error);
    }
  };

  onPictureTap = () => {
    const options = {
      title: I18n.t('users.add.picture.title'),
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, this.imageProcess);
  };

  imageProcess = async response => {
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else {
      this.menuPictureSmall = await ImageResizer.createResizedImage(
        response.uri,
        300,
        300,
        'JPEG',
        100,
        0,
        null,
      );

      this.menuPictureBig = await ImageResizer.createResizedImage(
        response.uri,
        600,
        600,
        'JPEG',
        100,
        0,
        null,
      );

      const source = {uri: this.menuPictureSmall.uri};

      this.setState({
        avatarSource: source,
      });
    }
  };

  render() {
    const {menuOption, avatarSource, editing} = this.state;
    const {product, description, price} = this.menu;
    return (
      <Container>
        <NavigationBar
          back
          title={menuOption.name}
          barStyle="yellow"
          editable
          onEdit={() =>
            this.setState(prevState => ({
              editing: !prevState.editing,
            }))
          }
        />
        <Grid>
          <Row size={2}>
            <Col style={styles.center}>
              <TouchableOpacity
                disabled={!editing}
                onPress={this.onPictureTap}
                style={styles.image}>
                <Image style={styles.image} source={avatarSource} />
                {editing && (
                  <Text style={styles.editImageLabel}>
                    {I18n.t('menu.add.editImage')}
                  </Text>
                )}
              </TouchableOpacity>
            </Col>
          </Row>
          <Row size={3}>
            <Col>
              <Formik
                initialValues={{
                  name: product,
                  description: description,
                  price: price,
                }}
                validationSchema={validation}
                onSubmit={this.onSubmit}>
                {({handleChange, handleBlur, handleSubmit, values, errors}) => {
                  // this.showError(errors);
                  return (
                    <View style={styles.formContainer}>
                      <Row>
                        <Col>
                          <InputCustom
                            label={I18n.t('menu.add.fields.name.label')}
                            placeholder={I18n.t(
                              'menu.add.fields.name.placeholder',
                            )}
                            value={values.name}
                            error={errors.name}
                            handleChange={handleChange('name')}
                            handleBlur={handleBlur('name')}
                            disabled={!editing}
                            underlined={editing}
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <InputCustom
                            label={I18n.t('menu.add.fields.description.label')}
                            placeholder={I18n.t(
                              'menu.add.fields.description.placeholder',
                            )}
                            value={values.description}
                            error={errors.description}
                            handleChange={handleChange('description')}
                            handleBlur={handleBlur('description')}
                            disabled={!editing}
                            underlined={editing}
                          />
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <InputCustom
                            label={I18n.t('menu.add.fields.price.label')}
                            placeholder={I18n.t(
                              'menu.add.fields.price.placeholder',
                            )}
                            value={values.price}
                            error={errors.price}
                            handleChange={handleChange('price')}
                            handleBlur={handleBlur('price')}
                            disabled={!editing}
                            underlined={editing}
                          />
                        </Col>
                      </Row>
                      <Row size={1}>
                        <Col>
                          {this.state.editing && (
                            <Button
                              rounded
                              warning
                              style={styles.button}
                              onPress={handleSubmit}>
                              <Text style={styles.buttonText}>
                                {I18n.t('menu.add.update')}
                              </Text>
                            </Button>
                          )}
                        </Col>
                      </Row>
                      <Row size={2} />
                    </View>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

MenuEdit.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default MenuEdit;
