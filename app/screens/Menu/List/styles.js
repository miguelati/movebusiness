import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../../themes';

export default EStyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    backgroundColor: colors.lightWhite,
    height: 60,
  },
  title: {
    ...fonts.rubikMedium32Black,
    marginLeft: 18,
  },
  buttonAddContainer: {
    alignItems: 'flex-end',
  },
  buttonAdd: {
    marginRight: 21,
  },
  imageBackground: {
    width: '100%',
    height: '100%',
  },
  menuTitle: {
    ...fonts.rubikLight32Black,
    marginLeft: 36,
    marginTop: 24,
  },
  imageAdd: {
    marginLeft: 28,
    marginTop: 21,
  },
  tabBarText: {
    ...fonts.rubikRegular19Black,
  },
});
