import React from 'react';
import PropTypes from 'prop-types';
import {Image, TouchableOpacity} from 'react-native';
import {Container, Grid, Row, Col, Text} from 'native-base';
import {
  NavigationBar,
  ListMenu,
  TitleScroll,
  SearchBoxBigger,
} from '../../../components';
import I18n from '../../../i18n';
import {MenuOptions, Menu} from '../../../models';
import styles from './styles';

class MenuList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      menuOptions: [],
    };
    const {navigation} = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      this.getAll();
    });
  }

  componentWillUnmount() {
    if (this.menuOptionsSanp) {
      this.menuOptionsSanp();
    }
  }

  getAll = () => {
    this.menuOptionsSanp = MenuOptions.onSnapshot(async snapshot => {
      const menuOptions = await this.processMenuOption(snapshot.docs);
      this.setState({menuOptions});
    });
  };

  processMenuOption = async docs => {
    const asyncRes = await Promise.all(
      docs.map(async item => {
        const data = await Menu.getAll(item.id);
        return {
          id: item.id,
          title: item.get('name'),
          data: this.processMenu(data.docs, item),
        };
      }),
    );

    return asyncRes;
  };

  processMenu = (docs, menuOptionRef) => {
    const menuOption = {
      id: menuOptionRef.id,
      name: menuOptionRef.get('name'),
      iconName: menuOptionRef.get('iconName'),
      source: menuOptionRef,
    };
    const menu = docs.map(item => ({
      id: item.id,
      product: item.get('name'),
      description: item.get('description'),
      price: item.get('price'),
      pictureSmall: item.get('pictureSmall'),
      pictureBig: item.get('pictureBig'),
      menuOption,
    }));

    return menu;
  };

  onPressAdd = () => {
    const {navigation} = this.props;
    navigation.navigate('MenuSelectMenuOption');
  };

  searchBoxChange = text => {
    console.log(text);
    // TODO: make searchBox work
  };

  onTitleScrollChanged = item => {
    this._listMenu.scrollToSection(item);
  };

  onItemSelected = item => {
    const {navigation} = this.props;
    navigation.navigate('MenuEdit', {menu: item, menuOption: item.menuOption});
  };

  render() {
    const {menuOptions} = this.state;
    return (
      <Container>
        <NavigationBar showMenu withTitle={false} />
        <Grid>
          <Row style={[styles.header, styles.center]}>
            <Col style={styles.titleContainer}>
              <Text style={styles.title}>{I18n.t('menu.list.title')}</Text>
            </Col>
            <Col style={styles.buttonAddContainer}>
              <TouchableOpacity onPress={this.onPressAdd}>
                <Image
                  style={styles.buttonAdd}
                  source={require('../../../commons/img/buttons/add.png')}
                />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={[styles.header, styles.center]}>
            <Col>
              <SearchBoxBigger
                placeholder={I18n.t('tables.list.searchPlaceholder')}
                onChange={this.searchBoxChange}
              />
            </Col>
          </Row>
          <Row size={4}>
            <Col>
              <Row size={1}>
                <Col>
                  <TitleScroll
                    items={menuOptions.map(item => item.title)}
                    onChange={this.onTitleScrollChanged}
                  />
                </Col>
              </Row>
              <Row size={10}>
                <Col>
                  <ListMenu
                    items={menuOptions}
                    onItemSelected={this.onItemSelected}
                    ref={component => (this._listMenu = component)}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

MenuList.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default MenuList;
