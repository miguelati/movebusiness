import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../../themes';

export default EStyleSheet.create({
  container: {
    backgroundColor: colors.yellow,
  },
  title: {
    ...fonts.notoSans32White,
    textAlign: 'left',
    marginLeft: 22,
  },
  icon: {
    color: colors.white,
  },
});
