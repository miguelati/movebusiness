import React from 'react';
import PropTypes from 'prop-types';
import {Container, Grid, Row, Col, Text} from 'native-base';
import {MenuOptions} from '../../../models';
import {NavigationBar, ListEditable} from '../../../components';
import I18n from '../../../i18n';
import styles from './styles';

class selectMenuOption extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      menuOptions: [],
    };

    this.menuOptions = MenuOptions.onSnapshot(this.onSnapshot);
  }

  onSnapshot = snap => {
    if (snap.docs.length > 0) {
      const {docs} = snap;
      this.setState({menuOptions: docs.map(item => this.process(item))});
    }
  };

  process = item => ({
    id: item.id,
    name: item.get('name'),
    iconName: item.get('iconName'),
    source: item,
  });

  listEditableItemSelected = item => {
    const {navigation} = this.props;
    navigation.navigate('MenuAdd', {menuOption: item});
  };

  showAddItem = () => {
    this.listEditable.showFieldAdd();
  };

  render() {
    const {menuOptions} = this.state;
    return (
      <Container style={styles.container}>
        <NavigationBar withTitle={false} back barStyle="yellow" />
        <Grid>
          <Row>
            <Col>
              <Text style={styles.title}>
                {I18n.t('menu.addCategory.title')}
              </Text>
            </Col>
          </Row>
          <Row size={10}>
            <Col>
              <ListEditable
                onItemSelected={this.listEditableItemSelected}
                items={menuOptions}
              />
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

selectMenuOption.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default selectMenuOption;
