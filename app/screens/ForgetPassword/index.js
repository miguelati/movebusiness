import React from 'react';
import PropTypes from 'prop-types';
import {Image, AsyncStorage} from 'react-native';
import {
  Container,
  Grid,
  Row,
  Col,
  Text,
  Item,
  Label,
  Input,
  Button,
  View,
  Toast,
} from 'native-base';
import {Formik} from 'formik';
import * as Yup from 'yup';
import firebase from 'react-native-firebase';
import {LoadingHUD} from 'react-native-hud-hybrid';

import styles from './styles';
import I18n from '../../i18n';

const validation = Yup.object().shape({
  email: Yup.string()
    .email(
      I18n.t('validation.invalid', {
        field: I18n.t('forgetPasswordScreen.fields.email'),
      }),
    )
    .required(
      I18n.t('validation.required', {
        field: I18n.t('forgetPasswordScreen.fields.email'),
      }),
    ),
});

class ForgetPassword extends React.PureComponent {
  constructor(props) {
    super(props);
    this.loadingHUD = new LoadingHUD();
  }
  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  goForgetPassword = () => {
    const {navigation} = this.props;
    navigation.navigate('ForgetPassword');
  };

  onSubmit = (values, actions) => {
    console.log(values);
    const {email} = values;
    actions.setSubmitting(true);
    this.loadingHUD.show();
    firebase
      .auth()
      .sendPasswordResetEmail(email)
      .then(() => {
        this.loadingHUD.hideAll();
        actions.setSubmitting(false);
        const {navigation} = this.props;
        navigation.goBack();
      })
      .catch(error => {
        this.loadingHUD.hideAll();
        actions.setSubmitting(false);
        let message = I18n.t(`forgetPasswordScreen.errors.${error.code}`);

        Toast.show({
          text: message,
          buttonText: '',
          duration: 3000,
          type: 'danger',
        });
      });
  };

  showError = errors => {
    let errorMessage = Object.keys(errors).map(item => errors[item]);

    if (errorMessage.length > 0) {
      Toast.show({
        text: errorMessage.join('\n'),
        buttonText: '',
        duration: 3000,
        type: 'danger',
      });
    } else {
      Toast.hide();
    }
  };

  render() {
    return (
      <Container>
        <Grid>
          <Row size={6}>
            <Col style={styles.center}>
              <Image source={require('../../commons/img/logo/padlock.png')} />
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.title}>
                {I18n.t('forgetPasswordScreen.title')}
              </Text>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.subTitle}>
                {I18n.t('forgetPasswordScreen.subtitle')}
              </Text>
            </Col>
          </Row>
          <Row size={4}>
            <Col>
              <Formik
                initialValues={{email: ''}}
                validationSchema={validation}
                onSubmit={this.onSubmit}>
                {({handleChange, handleBlur, handleSubmit, values, errors}) => {
                  this.showError(errors);
                  return (
                    <View style={styles.formContainer}>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.email !== undefined}>
                            <Label>
                              {I18n.t('forgetPasswordScreen.email')}
                            </Label>
                            <Input
                              autoCapitalize="none"
                              value={values.email}
                              onChangeText={handleChange('email')}
                              onBlur={handleBlur('email')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'forgetPasswordScreen.emailPlaceholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row size={3}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.button}
                            onPress={handleSubmit}>
                            <Text style={styles.buttonText}>
                              {I18n.t('forgetPasswordScreen.restore')}
                            </Text>
                          </Button>
                          <Button
                            transparent
                            style={styles.buttonLink}
                            onPress={this.goBack}>
                            <Text style={styles.link}>
                              {I18n.t('forgetPasswordScreen.back')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                    </View>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

ForgetPassword.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default ForgetPassword;
