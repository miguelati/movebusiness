import React from 'react';
import {ActivityIndicator, StatusBar, View} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {Base, Profile} from '../../models';

class AuthLoadingScreen extends React.Component {
  componentDidMount() {
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = JSON.parse(await AsyncStorage.getItem('userCredential'));
    let businessId = JSON.parse(await AsyncStorage.getItem('businessId'));

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    let toNavigate = 'Auth';
    let params = {};

    if (userToken) {
      const profile = new Profile(userToken.user.uid);
      const profileData = await profile.get();
      if (!businessId) {
        await AsyncStorage.setItem(
          'businessId',
          profileData.get('business').id,
        );
        businessId = profileData.get('business').id;
      }

      //const businessId = profileData.get('businessId');
      const permission =
        profileData.get('permission') &&
        profileData.get('permission') === 'admin';
      const userHasBusiness = profileData.get('business') !== undefined;
      params = permission ? {} : {message: 'El usuario no tiene permiso.'};
      if (permission && userHasBusiness) {
        Base.businessId = businessId;
        Profile.currentUserId = userToken.user.uid;
        toNavigate = 'App';
      } else if (permission && !userHasBusiness) {
        toNavigate = 'BussinessAdd';
      }
    }

    this.props.navigation.navigate(toNavigate, params);
  };

  // Render any loading content that you like here
  render() {
    return (
      <View>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

export default AuthLoadingScreen;
