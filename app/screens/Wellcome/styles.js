import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../themes';

export default EStyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    ...fonts.notoSans32White,
    marginLeft: 60,
    marginRight: 60,
  },
  titleYellow: {
    ...fonts.notoSans32Yellow,
  },
  button: {
    marginLeft: 60,
    marginRight: 60,
    justifyContent: 'center',
    borderRadius: 17,
    backgroundColor: colors.yellow,
  },
  buttonText: {
    ...fonts.rubikMedium18White,
  },
  buttonLink: {
    marginTop: 12,
    marginLeft: 60,
    marginRight: 60,
    borderRadius: 17,
    justifyContent: 'center',
    borderColor: colors.white,
  },
  buttonLinkText: {
    ...fonts.rubikRegular18White,
    color: colors.white,
  },
});
