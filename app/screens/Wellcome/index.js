import React from 'react';
import PropTypes from 'prop-types';
import {Image, ImageBackground} from 'react-native';
import {Container, Grid, Row, Col, Text, Button} from 'native-base';
import styles from './styles';
import I18n from '../../i18n';

class Wellcome extends React.PureComponent {
  goToLogin = () => {
    const {navigation} = this.props;
    navigation.navigate('Login');
  };

  goToSignUp = () => {
    const {navigation} = this.props;
    navigation.navigate('SignUp');
  };

  render() {
    return (
      <Container>
        <Grid>
          <Row>
            <Col style={styles.center}>
              <ImageBackground
                source={require('../../commons/img/backgrounds/login.png')}
                style={styles.background}>
                <Row size={3}>
                  <Col style={styles.center}>
                    <Image
                      source={require('../../commons/img/logo/logo-normal.png')}
                    />
                  </Col>
                </Row>
                <Row size={1}>
                  <Col>
                    <Text style={styles.title}>
                      {I18n.t('wellcomeScreen.title').toUpperCase()}
                      <Text style={styles.titleYellow}>
                        {I18n.t('wellcomeScreen.titleSpecial').toUpperCase()}
                      </Text>
                    </Text>
                  </Col>
                </Row>
                <Row size={2}>
                  <Col>
                    <Button
                      rounded
                      warning
                      style={styles.button}
                      onPress={this.goToLogin}>
                      <Text style={styles.buttonText}>
                        {I18n.t('wellcomeScreen.login')}
                      </Text>
                    </Button>
                    <Button
                      bordered
                      style={styles.buttonLink}
                      onPress={this.goToSignUp}>
                      <Text style={styles.buttonLinkText}>
                        {I18n.t('wellcomeScreen.signUp')}
                      </Text>
                    </Button>
                  </Col>
                </Row>
              </ImageBackground>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

Wellcome.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Wellcome;
