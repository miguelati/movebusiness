import React from 'react';
import PropTypes from 'prop-types';
import {Image, Keyboard} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  Container,
  Grid,
  Row,
  Col,
  Text,
  Item,
  Label,
  Input,
  Button,
  View,
  Toast,
} from 'native-base';
import {Formik} from 'formik';
import * as Yup from 'yup';
import firebase from 'react-native-firebase';
import {LoadingHUD} from 'react-native-hud-hybrid';

import styles from './styles';
import I18n from '../../i18n';
import {Base, Profile} from '../../models';

const validation = Yup.object().shape({
  email: Yup.string()
    .email(
      I18n.t('validation.invalid', {
        field: I18n.t('loginScreen.fields.email'),
      }),
    )
    .required(
      I18n.t('validation.required', {
        field: I18n.t('loginScreen.fields.email'),
      }),
    ),
  password: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('loginScreen.fields.password'),
    }),
  ),
});

class Login extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      toastPosition: 'bottom',
    };
    this.loadingHUD = new LoadingHUD();
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      this._keyboardDidShow.bind(this),
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      this._keyboardDidHide.bind(this),
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  _keyboardDidShow() {
    this.setState({toastPosition: 'top'});
  }

  _keyboardDidHide() {
    this.setState({toastPosition: 'bottom'});
  }

  goBack = () => {
    const {navigation} = this.props;
    navigation.goBack();
  };

  goForgetPassword = () => {
    const {navigation} = this.props;
    navigation.navigate('ForgetPassword');
  };

  onSubmit = async (values, actions) => {
    try {
      const {email, password} = values;
      actions.setSubmitting(true);
      this.loadingHUD.show();
      const userCredential = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);
      await AsyncStorage.setItem(
        'userCredential',
        JSON.stringify(userCredential),
      );

      const profile = new Profile(userCredential.user.uid);
      const prop = await profile.get();
      const storageValues = [
        ['userHasbusiness', JSON.stringify(prop.get('hasBusiness'))],
      ];
      if (prop.get('business') !== undefined) {
        const business = prop.get('business').id;
        Base.businessId = business;
        Profile.currentUserId = userCredential.user.uid;
        storageValues.push(['businessId', JSON.stringify(business)]);
      }
      await AsyncStorage.multiSet(storageValues);
      this.loadingHUD.hideAll();
      actions.setSubmitting(false);
      const {navigation} = this.props;
      navigation.navigate(prop.get('hasBusiness') ? 'App' : 'BussinessAdd');
    } catch (error) {
      console.log(error);
      this.loadingHUD.hideAll();
      actions.setSubmitting(false);
      let message = I18n.t(`loginScreen.errors.${error.code}`);

      Toast.show({
        text: message,
        buttonText: '',
        duration: 3000,
        type: 'danger',
      });
    }
  };

  showError = errors => {
    const {toastPosition} = this.state;
    let errorMessage = Object.keys(errors).map(item => errors[item]);

    if (errorMessage.length > 0) {
      Toast.show({
        text: errorMessage.join('\n'),
        buttonText: '',
        duration: 3000,
        type: 'danger',
        position: toastPosition,
      });
    } else {
      Toast.hide();
    }
  };

  render() {
    return (
      <Container>
        <Grid>
          <Row size={4}>
            <Col style={styles.center}>
              <Image
                source={require('../../commons/img/logo/logo-normal.png')}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.title}>{I18n.t('loginScreen.title')}</Text>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.subTitle}>
                {I18n.t('loginScreen.subTitle')}
              </Text>
            </Col>
          </Row>
          <Row size={5}>
            <Col>
              <Formik
                initialValues={{email: '', password: ''}}
                validationSchema={validation}
                onSubmit={this.onSubmit}>
                {({handleChange, handleBlur, handleSubmit, values, errors}) => {
                  this.showError(errors);
                  return (
                    <View style={styles.formContainer}>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.email !== undefined}>
                            <Label>{I18n.t('loginScreen.email')}</Label>
                            <Input
                              autoCapitalize="none"
                              value={values.email}
                              onChangeText={handleChange('email')}
                              onBlur={handleBlur('email')}
                              style={styles.input}
                              placeholder={I18n.t(
                                'loginScreen.emailPlaceholder',
                              )}
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row>
                        <Col>
                          <Item
                            stackedLabel
                            style={styles.item}
                            error={errors.password !== undefined}>
                            <Label>{I18n.t('loginScreen.password')}</Label>
                            <Input
                              value={values.password}
                              onChangeText={handleChange('password')}
                              onBlur={handleBlur('password')}
                              style={styles.input}
                              secureTextEntry
                            />
                          </Item>
                        </Col>
                      </Row>
                      <Row size={2}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.button}
                            onPress={handleSubmit}>
                            <Text style={styles.buttonText}>
                              {I18n.t('loginScreen.accept')}
                            </Text>
                          </Button>
                          <Button
                            transparent
                            style={styles.buttonLink}
                            onPress={this.goBack}>
                            <Text style={styles.link}>
                              {I18n.t('loginScreen.back')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row size={1}>
                        <Col>
                          <Button
                            transparent
                            style={styles.buttonForgot}
                            onPress={this.goForgetPassword}>
                            <Text style={styles.forgot}>
                              {I18n.t('loginScreen.forgotPassword')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                    </View>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

Login.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default Login;
