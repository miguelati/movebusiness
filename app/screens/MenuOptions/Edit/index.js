import React from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity, Image, Alert} from 'react-native';
import * as Yup from 'yup';
import {Formik} from 'formik';
import {View, Container, Grid, Row, Col, Button, Text} from 'native-base';
import HUD, {LoadingHUD} from 'react-native-hud-hybrid';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

import {MenuOptions} from '../../../models';
import {NavigationBar, InputCustom} from '../../../components';
import I18n from '../../../i18n';
import styles from './styles';
import AsyncStorage from '@react-native-community/async-storage';
import firebase from 'react-native-firebase';

const validation = Yup.object().shape({
  name: Yup.string().required(
    I18n.t('validation.required', {
      field: I18n.t('menuOptionsScreen.add.fields.name.name'),
    }),
  ),
});

class LocalEdit extends React.PureComponent {
  constructor(props) {
    super(props);
    const {navigation} = this.props;
    this.menuOption = navigation.state.params.menuOption;
    this.state = {
      menuOption: {
        name: this.menuOption.get('name'),
      },
      avatarSource: this.menuOption.get('pictureSmall')
        ? {uri: this.menuOption.get('pictureSmall')}
        : require('../../../commons/img/defaults/menuOption.png'),
    };
    this.loadingHUD = new LoadingHUD();
  }

  componentDidMount() {
    HUD.config({
      loadingText: 'cargando...',
    });
  }

  onRemove = async () => {
    Alert.alert(
      I18n.t('menuOptionsScreen.edit.removeMessage.title'),
      I18n.t('menuOptionsScreen.edit.removeMessage.message'),
      [
        {
          text: I18n.t('menuOptionsScreen.edit.removeMessage.yes'),
          onPress: async () => {
            this.loadingHUD.show();
            await MenuOptions.remove(this.menuOption.id);
            this.loadingHUD.hideAll();
            const {navigation} = this.props;
            navigation.goBack();
          },
        },
        {
          text: I18n.t('menuOptionsScreen.edit.removeMessage.no'),
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  };

  onSubmit = async values => {
    try {
      this.loadingHUD.show();
      const businessId = (await AsyncStorage.getItem('businessId')).replaceAll(
        '"',
        '',
      );
      const data = {
        ...values,
        businessId,
      };
      if (this.profilePictureSmall !== undefined) {
        const storageRef = firebase
          .storage()
          .ref(`menuOptions/pictures/${businessId}/${values.name}_small.jpg`);

        await storageRef.putFile(this.profilePictureSmall.uri);
        data.pictureSmall = await storageRef.getDownloadURL();
      }
      if (this.profilePictureBig !== undefined) {
        const storageRef = firebase
          .storage()
          .ref(`menuOptions/pictures/${businessId}/${values.name}_big.jpg`);

        await storageRef.putFile(this.profilePictureBig.uri);
        data.pictureBig = await storageRef.getDownloadURL();
      }
      await MenuOptions.setMerged(this.menuOption.id, data);
      this.loadingHUD.hideAll();
      const {navigation} = this.props;
      navigation.goBack();
    } catch (error) {
      console.log(error);
    }
  };

  onPictureTap = () => {
    const options = {
      title: I18n.t('users.edit.picture.title'),
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, this.imageProcess);
  };

  imageProcess = async response => {
    if (response.didCancel) {
      console.log('User cancelled image picker');
    } else if (response.error) {
      console.log('ImagePicker Error: ', response.error);
    } else {
      this.profilePictureSmall = await ImageResizer.createResizedImage(
        response.uri,
        300,
        300,
        'JPEG',
        100,
        0,
        null,
      );

      this.profilePictureBig = await ImageResizer.createResizedImage(
        response.uri,
        600,
        600,
        'JPEG',
        100,
        0,
        null,
      );

      const source = {uri: this.profilePictureSmall.uri};

      this.setState({
        avatarSource: source,
      });
    }
  };

  render() {
    const {menuOption, avatarSource} = this.state;
    return (
      <Container>
        <NavigationBar
          back
          barStyle="yellow"
          title="menuOptionsScreen.edit.navigationBarTitile"
        />
        <Grid>
          <Row size={2}>
            <Col style={styles.center}>
              <TouchableOpacity
                onPress={this.onPictureTap}
                style={styles.image}>
                <Image style={styles.image} source={avatarSource} />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row size={3}>
            <Col>
              <Formik
                initialValues={menuOption}
                validationSchema={validation}
                onSubmit={this.onSubmit}>
                {({handleChange, handleBlur, handleSubmit, values, errors}) => {
                  // this.showError(errors);
                  return (
                    <View style={styles.formContainer}>
                      <Row>
                        <Col>
                          <InputCustom
                            label={I18n.t(
                              'menuOptionsScreen.edit.fields.name.label',
                            )}
                            placeholder={I18n.t(
                              'menuOptionsScreen.edit.fields.name.placeholder',
                            )}
                            value={values.name}
                            error={errors.name}
                            handleChange={handleChange('name')}
                            handleBlur={handleBlur('name')}
                          />
                        </Col>
                      </Row>
                      <Row size={1}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.button}
                            onPress={handleSubmit}>
                            <Text style={styles.buttonText}>
                              {I18n.t('menuOptionsScreen.edit.save')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row size={1}>
                        <Col>
                          <Button
                            rounded
                            warning
                            style={styles.buttonRemove}
                            onPress={this.onRemove}>
                            <Text style={styles.buttonText}>
                              {I18n.t('menuOptionsScreen.edit.remove')}
                            </Text>
                          </Button>
                        </Col>
                      </Row>
                      <Row size={2} />
                    </View>
                  );
                }}
              </Formik>
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

LocalEdit.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default LocalEdit;
