import EStyleSheet from 'react-native-extended-stylesheet';
import {fonts, colors} from '../../../themes';

export default EStyleSheet.create({
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  header: {
    backgroundColor: colors.lightWhite,
    height: 60,
  },
  title: {
    ...fonts.rubikMedium26White,
    color: colors.black,
    marginLeft: 18,
  },
  titleContainer: {},
  buttonAddContainer: {
    alignItems: 'flex-end',
  },
  buttonAdd: {
    marginRight: 21,
  },
});
