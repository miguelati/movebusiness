import React from 'react';
import PropTypes from 'prop-types';
import {Image, TouchableOpacity} from 'react-native';
import {Container, Grid, Row, Col, Text} from 'native-base';
import {
  NavigationBar,
  SearchBoxBigger,
  ListMenuOptions,
} from '../../../components';
import {MenuOptions} from '../../../models';
import I18n from '../../../i18n';
import styles from './styles';

class MenuOptionsList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      menuOptions: [],
      menuOptionsOriginal: [],
    };
    this.unsuscribe = MenuOptions.onSnapshot(snapshot => {
      const menuOptions = this.processData(snapshot.docs);
      this.setState({menuOptions, menuOptionsOriginal: menuOptions});
    });
  }

  componentWillUnmount() {
    this.unsuscribe();
  }

  processData = menuOptions => {
    return menuOptions.map(item => ({
      id: item.id,
      name: item.get('name'),
      iconName: item.get('iconName'),
      pictureSmall: item.get('pictureSmall'),
      pictureBig: item.get('pictureBig'),
      source: item,
    }));
  };

  addButtonHandler = () => {
    const {navigation} = this.props;
    navigation.navigate('MenuOptionsAdd');
  };

  searchBoxChange = text => {
    if (text === '') {
      this.setState(prevState => ({
        menuOptions: prevState.menuOptionsOriginal,
      }));
    } else {
      this.setState(prevState => {
        return {
          menuOptions: prevState.menuOptionsOriginal.filter(
            item => item.name.indexOf(text) !== -1,
          ),
        };
      });
    }
  };

  onItemPress = item => {
    const {navigation} = this.props;
    navigation.navigate('MenuOptionsEdit', {menuOption: item.source});
  };

  render() {
    const {menuOptions} = this.state;
    return (
      <Container>
        <NavigationBar showMenu withTitle={false} />
        <Grid>
          <Row style={[styles.header, styles.center]}>
            <Col size={4} style={styles.titleContainer}>
              <Text style={styles.title} maxFontSizeMultiplier={2}>
                {I18n.t('menuOptionsScreen.list.title')}
              </Text>
            </Col>
            <Col size={1} style={styles.buttonAddContainer}>
              <TouchableOpacity onPress={this.addButtonHandler}>
                <Image
                  style={styles.buttonAdd}
                  source={require('../../../commons/img/buttons/add.png')}
                />
              </TouchableOpacity>
            </Col>
          </Row>
          <Row style={[styles.header, styles.center]}>
            <Col>
              <SearchBoxBigger
                placeholder={I18n.t('menuOptionsScreen.list.searchPlaceholder')}
                onChange={this.searchBoxChange}
              />
            </Col>
          </Row>
          <Row>
            <Col>
              <ListMenuOptions
                items={menuOptions}
                onItemPress={this.onItemPress}
              />
            </Col>
          </Row>
        </Grid>
      </Container>
    );
  }
}

MenuOptionsList.propTypes = {
  navigation: PropTypes.object.isRequired,
};

export default MenuOptionsList;
