import React from 'react';
import EStyleSheet from 'react-native-extended-stylesheet';
import {SafeAreaView} from 'react-native';
import {StyleProvider, Root} from 'native-base';
import Navigator from './config/routes';
import getTheme from '../native-base-theme/components';
import HUD from 'react-native-hud-hybrid';

// import { colors } from './themes';

HUD.config({
  // backgroundColor: '#BB000000',
  // tintColor: '#FFFFFF',
  // cornerRadius: 5, // only for android
  // duration: 2000,
  // graceTime: 3000,
  // minShowTime: 800,
  // dimAmount: 0.0, // only for andriod
  loadingText: 'Cargando...',
});

EStyleSheet.build({});

export default () => (
  <StyleProvider style={getTheme()}>
    <Root>
      <Navigator />
    </Root>
  </StyleProvider>
);
