import React from 'react';

import {createIconSetFromFontello} from 'react-native-vector-icons';
import fontelloConfig from './icons.json';
const Icon = createIconSetFromFontello(fontelloConfig);

import {createSwitchNavigator, createAppContainer} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import {
  SplashStack,
  AuthStack,
  UsersStack,
  BussinesStack,
  MenuStack,
  TablesStack,
  LocalsStack,
  MenuOptionsStack,
} from './stacks';

import {Drawer, Tabbar} from '../components';

import {AuthLoading} from '../screens';

// create tabbar
const TabStack = createBottomTabNavigator(
  {
    Users: UsersStack,
    Menu: MenuStack,
    Tables: TablesStack,
  },
  {
    tabBarPosition: 'bottom',
    tabBarComponent: props => <Tabbar navigationOptions={props} />,
    navigationOptions: {
      drawerIcon: () => {
        return <Icon style={{fontSize: 25, color: '#FFF'}} name="profile" />;
      },
    },
  },
);

const DrawerStack = createDrawerNavigator(
  {
    Home: TabStack,
    Bussines: BussinesStack,
    Locals: LocalsStack,
    MenuOptions: MenuOptionsStack,
  },
  {
    contentComponent: props => <Drawer {...props} />,
  },
);

// create our app's navigation stack
const AppNavigator = createSwitchNavigator(
  {
    AuthLoading,
    Splash: SplashStack,
    Auth: AuthStack,
    App: DrawerStack,
  },
  {
    initialRouteName: 'AuthLoading',
  },
);

export default createAppContainer(AppNavigator);
