import SplashStack from './splash';
import AuthStack from './auth';
import BussinesStack from './bussines';
import UsersStack from './users';
import MenuStack from './menu';
import TablesStack from './tables';
import LocalsStack from './locals';
import MenuOptionsStack from './menuOptions';

export {
  SplashStack,
  AuthStack,
  BussinesStack,
  UsersStack,
  MenuStack,
  TablesStack,
  LocalsStack,
  MenuOptionsStack,
};
