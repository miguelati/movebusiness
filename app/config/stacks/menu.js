import React from 'react';
import {Image} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import {Menu} from '../../screens';
import noHeader from './noHeader';

const stack = createStackNavigator(
  {
    MenuList: Menu.List,
    MenuSelectMenuOption: Menu.SelectMenuOption,
    MenuAdd: Menu.Add,
    MenuEdit: Menu.Edit,
  },
  {...noHeader},
);

const screen = {
  screen: stack,
  navigationOptions: {
    drawerIcon: () => {
      return <Image source={require('../../commons/img/menu/icon-menu.png')} />;
    },
  },
};

export default screen;
