const noHeader = {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  },
};

export default noHeader;
