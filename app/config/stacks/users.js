import React from 'react';
import {Image} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import {Users} from '../../screens';
import noHeader from './noHeader';

const UsersStack = createStackNavigator(
  {
    UsersList: Users.List,
    UsersAdd: Users.Add,
    UserEdit: Users.Edit,
    // UserHistory: Users.History,
  },
  {...noHeader},
);

const screenUsers = {
  screen: UsersStack,
  navigationOptions: {
    drawerIcon: () => {
      return <Image source={require('../../commons/img/menu/icon-user.png')} />;
    },
  },
};

export default screenUsers;
