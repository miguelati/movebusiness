import {createStackNavigator} from 'react-navigation-stack';
import {Wellcome, SignUp, Login, ForgetPassword, Bussines} from '../../screens';
import noHeader from './noHeader';

const modal = {
  mode: 'modal',
};

const AuthStack = createStackNavigator(
  {
    Wellcome,
    Login,
    ForgetPassword,
    SignUp,
    BussinessAdd: Bussines.Add,
  },
  {...noHeader, ...modal},
);

export default AuthStack;
