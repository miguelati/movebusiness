import {createStackNavigator} from 'react-navigation-stack';
import {Tables} from '../../screens';
import noHeader from './noHeader';

const TablesStack = createStackNavigator(
  {
    TablesList: Tables.List,
    TablesAdd: Tables.Add,
  },
  {...noHeader},
);

const screenTables = {
  screen: TablesStack,
};

export default screenTables;
