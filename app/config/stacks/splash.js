import {createStackNavigator} from 'react-navigation-stack';

import {Splash} from '../../screens';
import noHeader from './noHeader';

const modal = {
  mode: 'modal',
};

const SplashStack = createStackNavigator(
  {
    Splash,
  },
  {...noHeader, ...modal},
);

export default SplashStack;
