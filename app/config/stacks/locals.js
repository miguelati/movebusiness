import React from 'react';
import {Image} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import I18n from '../../i18n';
import {Locals} from '../../screens';
import noHeader from './noHeader';

const stack = createStackNavigator(
  {
    LocalsList: Locals.List,
    LocalAdd: Locals.Add,
    LocalEdit: Locals.Edit,
  },
  {
    ...noHeader,
  },
);

const BussinesStack = {
  screen: stack,
  navigationOptions: {
    drawerLabel: I18n.t('drawer.items.branchOffices'),
    drawerIcon: () => {
      return (
        <Image
          source={require('../../commons/img/menu/icon-branchOffices.png')}
        />
      );
    },
  },
};

export default BussinesStack;
