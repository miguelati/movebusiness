import React from 'react';
import {Image} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import I18n from '../../i18n';
import {Bussines} from '../../screens';
import noHeader from './noHeader';

const stack = createStackNavigator(
  {
    BussinesUpdate: Bussines.Update,
    BussinesAdd: Bussines.Add,
  },
  {
    ...noHeader,
  },
);

const BussinesStack = {
  screen: stack,
  navigationOptions: {
    drawerLabel: I18n.t('drawer.items.bussines'),
    drawerIcon: () => {
      return (
        <Image source={require('../../commons/img/menu/icon-bussines.png')} />
      );
    },
  },
};

export default BussinesStack;
