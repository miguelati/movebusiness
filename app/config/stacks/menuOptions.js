import React from 'react';
import {Image} from 'react-native';
import {createStackNavigator} from 'react-navigation-stack';
import I18n from '../../i18n';
import {MenuOptions} from '../../screens';
import noHeader from './noHeader';

const stack = createStackNavigator(
  {
    MenuOptionsList: MenuOptions.List,
    MenuOptionsAdd: MenuOptions.Add,
    MenuOptionsEdit: MenuOptions.Edit,
  },
  {
    ...noHeader,
  },
);

const MenuOptionsStack = {
  screen: stack,
  navigationOptions: {
    drawerLabel: I18n.t('drawer.items.menuOptions'),
    drawerIcon: () => {
      return <Image source={require('../../commons/img/menu/icon-menu.png')} />;
    },
  },
};

export default MenuOptionsStack;
