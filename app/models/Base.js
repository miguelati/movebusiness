import Firebase from 'react-native-firebase';

const Firestore = Firebase.firestore();

class Base {
  static businessId = null;

  constructor(docId) {
    this.docId = Base.checkCollection(docId);
  }

  update = (fields = {}) => {
    return Firestore.doc(this.docId).update(fields);
  };

  get = () => {
    return Firestore.doc(this.docId).get();
  };

  static checkCollection = collection => {
    return collection.replace('${businessId}', this.businessId);
  };

  static getAll(collection) {
    return Firestore.collection(this.checkCollection(collection)).get();
  }

  static add(collection, fields = {}) {
    return Firestore.collection(this.checkCollection(collection)).add(fields);
  }

  static set(collection, docId, fields = {}, options = {}) {
    return Firestore.doc(`${this.checkCollection(collection)}/${docId}`).set(
      fields,
      options,
    );
  }

  static getRef(collection, docId) {
    const collectionProcessed = this.checkCollection(collection);
    if (docId === null) {
      return Firestore.collection(`${collectionProcessed}`);
    } else {
      return Firestore.doc(`${collectionProcessed}/${docId}`);
    }
  }

  static onSnapshot(collection, callback) {
    return Firestore.collection(
      `${this.checkCollection(collection)}`,
    ).onSnapshot(callback);
  }

  static remove(collection, docId) {
    return Firestore.doc(
      `${this.checkCollection(collection)}/${docId}`,
    ).delete();
  }
}

export default Base;
