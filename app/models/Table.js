import Base from './Base';

class Table extends Base {
  static modelName = 'business/${businessId}/locals/${localId}/menu';

  constructor(localId, doc) {
    const modelName = this.getModelName(localId);
    super(`${modelName}/${doc}`);
  }

  prop = field => this.properties[field];

  static getModelName = localId => {
    return this.modelName.replace('${localId}', localId);
  };

  static add(localId, fields = {}) {
    const modelName = this.getModelName(localId);
    return Base.add(modelName, fields);
  }

  static setMerged(localId, docId, fileds = {}) {
    const modelName = this.getModelName(localId);
    return Base.set(modelName, docId, fileds, {merge: true});
  }

  static getRef(localId, docId = null) {
    const modelName = this.getModelName(localId);
    return Base.getRef(modelName, docId);
  }

  static onSnapshot(localId, callback) {
    const modelName = this.getModelName(localId);
    return Base.onSnapshot(modelName, callback);
  }

  static getAll(localId) {
    const modelName = this.getModelName(localId);
    return Base.getAll(modelName);
  }

  static remove(localId, docId) {
    const modelName = this.getModelName(localId);
    return Base.remove(modelName, docId);
  }
}

export default Table;
