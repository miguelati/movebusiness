import Base from './Base';

class MenuOptions extends Base {
  static modelName = 'business/${businessId}/menuOptions';

  constructor(doc) {
    super(`${this.modelName}/${doc}`);
  }

  prop = field => this.properties[field];

  static add(fields = {}) {
    return Base.add(this.modelName, fields);
  }

  static setMerged(docId, fileds = {}) {
    return Base.set(this.modelName, docId, fileds, {merge: true});
  }

  static getRef(docId = null) {
    return Base.getRef(this.modelName, docId);
  }

  static onSnapshot(callback) {
    return Base.onSnapshot(this.modelName, callback);
  }

  static remove(docId) {
    return Base.remove(this.modelName, docId);
  }
}

export default MenuOptions;
