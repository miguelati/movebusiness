import Base from './Base';

class Local extends Base {
  static modelName = 'business/${businessId}/locals';

  constructor(doc) {
    super(`${Local.modelName}/${doc}`);
  }

  prop = field => this.properties[field];

  static getModelName = businessId => {
    return this.modelName.replace('${businessId}', businessId);
  };

  static add(fields = {}) {
    return Base.add(Local.modelName, fields);
  }

  static setMerged(docId, fileds = {}) {
    return Base.set(Local.modelName, docId, fileds, {merge: true});
  }

  static getRef(docId = null) {
    return Base.getRef(Local.modelName, docId);
  }

  static onSnapshot(businessId, callback) {
    const modelName = this.getModelName(businessId);
    return Base.onSnapshot(modelName, callback);
  }

  static remove(docId) {
    return Base.remove(Local.modelName, docId);
  }
}

export default Local;
