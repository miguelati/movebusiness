import Base from './Base';

class Menu extends Base {
  static modelName = 'business/${businessId}/menuOptions/${menuOptionId}/menu';

  constructor(menuOptionId, doc) {
    const modelName = this.getModelName(menuOptionId);
    super(`${modelName}/${doc}`);
  }

  prop = field => this.properties[field];

  static getModelName = menuOptionId => {
    return this.modelName.replace('${menuOptionId}', menuOptionId);
  };

  static add(menuOptionId, fields = {}) {
    const modelName = this.getModelName(menuOptionId);
    return Base.add(modelName, fields);
  }

  static setMerged(menuOptionId, docId, fileds = {}) {
    const modelName = this.getModelName(menuOptionId);
    return Base.set(modelName, docId, fileds, {merge: true});
  }

  static getRef(menuOptionId, docId = null) {
    const modelName = this.getModelName(menuOptionId);
    return Base.getRef(modelName, docId);
  }

  static onSnapshot(menuOptionId, callback) {
    const modelName = this.getModelName(menuOptionId);
    return Base.onSnapshot(modelName, callback);
  }

  static getAll(menuOptionId) {
    const modelName = this.getModelName(menuOptionId);
    return Base.getAll(modelName);
  }

  static remove(menuOptionId, docId) {
    const modelName = this.getModelName(menuOptionId);
    return Base.remove(modelName, docId);
  }
}

export default Menu;
