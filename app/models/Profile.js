import Base from './Base';
import Business from './Business';
import firebase from 'react-native-firebase';

class Profile extends Base {
  static currentUserId = null;
  static modelName = 'profiles';

  constructor(doc) {
    super(`${Profile.modelName}/${doc}`);
  }

  prop = field => this.properties[field];

  static add(fields = {}) {
    return Base.add(Profile.modelName, fields);
  }

  static setMerged(docId, fileds = {}) {
    return Base.set(Profile.modelName, docId, fileds, {merge: true});
  }

  static getRef(docId = null) {
    return Base.getRef(Profile.modelName, docId);
  }

  static onSnapshot(callback) {
    return Base.onSnapshot(Profile.modelName, callback);
  }

  static getByBusiness(businessId, callback) {
    const businessRef = Business.getRef(businessId);
    const profileRef = Profile.getRef();
    return profileRef.where('business', '==', businessRef).onSnapshot(callback);
  }

  static remove(docId) {
    return Base.remove(Profile.modelName, docId);
  }
}

export default Profile;
