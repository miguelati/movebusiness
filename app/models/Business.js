import Base from './Base';

class Business extends Base {
  static modelName = 'business';

  static add(fields = {}) {
    return Base.add(Business.modelName, fields);
  }

  static setMerged(docId, fields = {}) {
    return Base.set(Business.modelName, docId, fields, {merge: true});
  }

  static getRef(docId = null) {
    return Base.getRef(Business.modelName, docId);
  }
}

export default Business;
