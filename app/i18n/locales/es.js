export default {
  common: {
    bool: {
      yes: 'Si',
      no: 'No',
    },
  },
  drawer: {
    title: 'Menu',
    items: {
      home: 'Inicio',
      bussines: 'Empresa',
      users: 'Usuarios',
      branchOffices: 'Locales',
      menuOptions: 'Opciones del menú',
      logout: 'Cerrar sesión',
    },
  },
  navigationBar: {
    title: 'Inicio',
    buttons: {
      back: 'Volver',
    },
  },
  validation: {
    invalid: '%{field} invalid',
    required: '%{field} requerido',
  },
  wellcomeScreen: {
    title: 'Bienvenido a\nMOVE ',
    titleSpecial: 'Negocio',
    login: 'Login',
    signUp: 'SingUp',
  },
  loginScreen: {
    title: 'Registro',
    subTitle: 'Por favor, registrese para continuar',
    email: 'Email:',
    emailPlaceholder: 'ej: juan.perez@mail.com',
    password: 'Contraseña:',
    accept: 'Aceptar',
    back: 'Volver',
    forgotPassword: '¿Olvidaste la contraseña?',
    fields: {
      email: 'Email',
      password: 'Contraseña',
    },
    errors: {
      'auth/user-disabled': 'Usuario no encontrado o deshabilitado',
      'auth/invalid-email': 'El email o la contraseña no son válidos',
      'auth/user-not-found': 'Usuario no encontrado o deshabilitado',
      'auth/wrong-password': 'El email o la contraseña no son válidos',
      default: 'Ha ocurrido un error, intente más tarde',
    },
  },
  signUpScreen: {
    title: 'SignUp',
    subTitle: 'Por favor registrese para continuar',
    firstName: 'Nombre:',
    firstNamePlaceholder: 'Juan',
    lastName: 'Apellido:',
    lastNamePlaceholder: 'Pérez',
    email: 'Email:',
    emailPlaceholder: 'ej: juan.perez@mail.com',
    password: 'Contraseña:',
    accept: 'Aceptar',
    back: 'Volver',
    fields: {
      firstName: 'Nombre',
      lastName: 'Apellido',
      email: 'Email',
      password: 'Contraseña',
    },
    errors: {
      'auth/email-already-in-use': 'El email ya está en uso',
      'auth/invalid-email': 'El email no es válido',
      'auth/operation-not-allowed': 'Operación no permitida',
      'auth/weak-password': 'El password es muy sencillo',
      default: 'Ha ocurrido un error, intente más tarde',
    },
  },
  users: {
    list: {
      title: 'Usuarios',
      searchPlaceholder: 'Buscar usuario',
      permissions: {
        admin: 'Administrador',
        chef: 'Cocinero',
        waiter: 'Mozo',
      },
    },
    add: {
      navigationBarTitile: 'Agregar',
      picture: {
        title: 'Selecciona',
      },
      fields: {
        firstName: {
          label: 'Nombre:',
          name: 'Nombre',
          placeholder: 'Ej. Juan',
        },
        lastName: {
          label: 'Apellido:',
          name: 'Apellido',
          placeholder: 'Ej. Pérez',
        },
        email: {
          label: 'Email:',
          name: 'Email',
          placeholder: 'Ej. jperez@mail.com',
        },
        cellphone: {
          label: 'Celular:',
          name: 'Celular',
          placeholder: 'Ej. 0981333444',
        },
        local: {
          label: 'Local:',
          name: 'Local',
          placeholder: 'Seleccione un local',
        },
        permission: {
          label: 'Permiso:',
          name: 'Permiso',
        },
      },
      save: 'Guardar',
    },
    edit: {
      navigationBarTitile: 'Editar',
      picture: {
        title: 'Selecciona',
      },
      fields: {
        firstName: {
          label: 'Nombre:',
          name: 'Nombre',
          placeholder: 'Ej. Juan',
        },
        lastName: {
          label: 'Apellido:',
          name: 'Apellido',
          placeholder: 'Ej. Pérez',
        },
        email: {
          label: 'Email:',
          name: 'Email',
          placeholder: 'Ej. jperez@mail.com',
        },
        cellphone: {
          label: 'Celular:',
          name: 'Celular',
          placeholder: 'Ej. 0981333444',
        },
        permission: {
          label: 'Permiso:',
          name: 'Permiso',
        },
      },
      save: 'Guardar',
      remove: 'Eliminar',
      removeMessage: {
        title: 'Eliminar',
        message: 'Está seguro?',
        yes: 'Si',
        no: 'No',
      },
    },
  },
  menu: {
    list: {
      title: 'Menu',
    },
    addCategory: {
      title: 'Selecciona una opción',
    },
    add: {
      navigationBarTitile: 'Agregar plato',
      fields: {
        name: {
          label: 'Nombre:',
          name: 'Nombre',
          placeholder: 'Ex. Ensalada césar',
        },
        description: {
          label: 'Descripción:',
          name: 'Descripción',
          placeholder: 'Ex. cuenta con tomate, etc.',
        },
        price: {
          label: 'Precio:',
          name: 'Precio',
          placeholder: 'Ex. 10.000',
        },
      },
      editImage: 'Click para cambiar imagen',
      save: 'Agregar menu',
      update: 'Actualizar menu',
    },
  },
  forgetPasswordScreen: {
    title: 'Restablecer contraseña',
    subtitle: 'Introduce tu email para recuperar tu contraseña',
    email: 'Email:',
    emailPlaceholder: 'ej: juan.perez@mail.com',
    restore: 'Restaurar',
    back: 'Volver',
    fields: {
      email: 'Email',
    },
    errors: {
      'auth/invalid-email': 'El email no se ha encontrado o es inválido',
      'auth/missing-android-pkg-name':
        'Ha ocurrido un error, intente más tarde',
      'auth/missing-continue-uri': 'Ha ocurrido un error, intente más tarde',
      'auth/missing-ios-bundle-id': 'Ha ocurrido un error, intente más tarde',
      'auth/invalid-continue-uri': 'Ha ocurrido un error, intente más tarde',
      'auth/unauthorized-continue-uri':
        'Ha ocurrido un error, intente más tarde',
      'auth/user-not-found': 'El email no se ha encontrado o es inválido',
      default: 'Ha ocurrido un error, intente más tarde',
    },
  },
  tables: {
    list: {
      title: 'Mesas',
      searchPlaceholder: 'Buscar mesa',
      table: 'Mesa',
      chairs: '%{chairs} Asientos',
      smokingArea: 'Area de fumadores: %{can}',
    },
    add: {
      navigationBarTitile: 'Agregar',
      fields: {
        tableNumber: {
          label: 'número de mesa:',
          name: 'número de mesa',
          placeholder: 'Ej. 01',
        },
        totalChairs: {
          label: 'Total de asientos:',
          name: 'Total de asientos',
          placeholder: 'Ej. 6',
        },
        smokingArea: {
          label: 'Area de fumadores',
          name: 'Area de fumadores',
          placeholder: '',
        },
      },
      save: 'Guardar',
    },
  },
  businessScreen: {
    add: {
      navigationBarTitile: 'Empresa',
      fields: {
        name: {
          label: 'Nombre:',
          name: 'Nombre',
          placeholder: 'Ej. Otro Bar',
        },
        address: {
          label: 'Dirección:',
          name: 'Dirección',
          placeholder: 'Ej. San Martin 2045',
        },
        phone: {
          label: 'Teléfonos',
          name: 'Teléfonos',
          placeholder: 'Ej. 224-344',
        },
      },
      save: 'Guardar',
    },
    update: {
      navigationBarTitile: 'Empresa',
      fields: {
        name: {
          label: 'Nombre:',
          name: 'Nombre',
          placeholder: 'Ej. Otro Bar',
        },
        address: {
          label: 'Dirección:',
          name: 'Dirección',
          placeholder: 'Ej. San Martin 2045',
        },
        phone: {
          label: 'Teléfonos',
          name: 'Teléfonos',
          placeholder: 'Ej. 224-344',
        },
      },
      save: 'Actualizar',
    },
  },
  localsScreen: {
    list: {
      title: 'Locales',
      searchPlaceholder: 'Buscar',
      local: 'Local',
    },
    add: {
      title: 'Agregar',
      fields: {
        name: {
          label: 'Nombre:',
          name: 'Nombre',
          placeholder: 'Ej. Sucursal Uno',
        },
        address: {
          label: 'Dirección:',
          name: 'Dirección',
          placeholder: 'Ej. Mcal. López 1232',
        },
        phone: {
          label: 'Teléfono:',
          name: 'Teléfono',
          placeholder: 'Ej. 021223223',
        },
      },
      save: 'Agregar',
    },
    edit: {
      title: 'Editar',
      fields: {
        name: {
          label: 'Nombre:',
          name: 'Nombre',
          placeholder: 'Ej. Sucursal Uno',
        },
        address: {
          label: 'Dirección:',
          name: 'Dirección',
          placeholder: 'Ej. Mcal. López 1232',
        },
        phone: {
          label: 'Teléfono:',
          name: 'Teléfono',
          placeholder: 'Ej. 021223223',
        },
      },
      save: 'Actualizar',
      remove: 'Eliminar',
      removeMessage: {
        title: 'Eliminar esto',
        message: 'Estás seguro?',
        yes: 'Si',
        no: 'No',
      },
    },
  },
  menuOptionsScreen: {
    list: {
      title: 'Opciones del menú',
      searchPlaceholder: 'Buscar',
      local: 'Local',
    },
    add: {
      title: 'Agregar',
      fields: {
        name: {
          label: 'Nombre:',
          name: 'Nombre',
          placeholder: 'Ej. Ensaladas',
        },
      },
      save: 'Agregar',
    },
    edit: {
      title: 'Editar',
      fields: {
        name: {
          label: 'Nombre:',
          name: 'Nombre',
          placeholder: 'Ej. Ensaladas',
        },
      },
      save: 'Actualizar',
      remove: 'Eliminar',
      removeMessage: {
        title: 'Eliminar esto',
        message: 'Estás seguro?',
        yes: 'Si',
        no: 'No',
      },
    },
  },
};
