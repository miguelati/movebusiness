export default {
  common: {
    bool: {
      yes: 'Yes',
      no: 'No',
    },
  },
  drawer: {
    title: 'Menu',
    items: {
      home: 'Home',
      bussines: 'Bussines',
      users: 'Users',
      branchOffices: 'Locals',
      menuOptions: 'Menu Options',
      logout: 'Logout',
    },
  },
  navigationBar: {
    title: 'Home',
    buttons: {
      back: 'Back',
    },
  },
  validation: {
    invalid: '%{field} invalid',
    required: '%{field} required',
  },
  wellcomeScreen: {
    title: 'Wellcom to\nMOVE ',
    titleSpecial: 'Bussines',
    login: 'Login',
    signUp: 'SingUp',
  },
  loginScreen: {
    title: 'Register',
    subTitle: 'Please, sign in to continue',
    email: 'E-mail:',
    emailPlaceholder: 'example: john@mail.com',
    password: 'Password:',
    accept: 'To accept',
    back: 'Back',
    forgotPassword: 'Did you forget the password?',
    fields: {
      email: 'E-mail',
      password: 'Password',
    },
    errors: {
      'auth/user-disabled': 'User not found or disabled',
      'auth/invalid-email': 'Email or password are invalid',
      'auth/user-not-found': 'User not found or disabled',
      'auth/wrong-password': 'Email or password are invalid',
      default: 'An error occurred, try againg later',
    },
  },
  signUpScreen: {
    title: 'SignUp',
    subTitle: 'Please sign up to continue',
    firstName: 'First name:',
    firstNamePlaceholder: 'Joe',
    lastName: 'Last name:',
    lastNamePlaceholder: 'Black',
    email: 'Email:',
    emailPlaceholder: 'example: Joeblack@mail.com',
    password: 'Password:',
    accept: 'To accept',
    back: 'Back',
    fields: {
      firstName: 'First name',
      lastName: 'Last name',
      email: 'E-mail',
      password: 'Password',
    },
    errors: {
      'auth/email-already-in-use': 'Email already exists',
      'auth/invalid-email': 'Email is invalid',
      'auth/operation-not-allowed': 'Operation not allowed',
      'auth/weak-password': 'Password is weak',
      default: 'An error occurred, try againg later',
    },
  },
  users: {
    list: {
      title: 'Users',
      searchPlaceholder: 'Search user',
      permissions: {
        admin: 'Administrator',
        chef: 'Chef',
        waiter: 'Waiter',
      },
    },
    add: {
      navigationBarTitile: 'Add',
      fields: {
        firstName: {
          label: 'First name:',
          name: 'First name',
          placeholder: 'Ex. Jhon',
        },
        lastName: {
          label: 'Last name:',
          name: 'Last name',
          placeholder: 'Ex. Walters',
        },
        email: {
          label: 'E-mail',
          name: 'E-mail',
          placeholder: 'Ex. jwalters@mail.com',
        },
        cellphone: {
          label: 'Cellphone:',
          name: 'Cellphone',
          placeholder: 'Ex. 0981333444',
        },
        local: {
          label: 'Local:',
          name: 'Local',
          placeholder: 'Select a local',
        },
        permission: {
          label: 'Permission:',
          name: 'Permission',
        },
      },
      save: 'Save',
    },
    edit: {
      navigationBarTitile: 'Edit',
      fields: {
        firstName: {
          label: 'First name:',
          name: 'First name',
          placeholder: 'Ex. Jhon',
        },
        lastName: {
          label: 'Last name:',
          name: 'Last name',
          placeholder: 'Ex. Walters',
        },
        email: {
          label: 'E-mail',
          name: 'E-mail',
          placeholder: 'Ex. jwalters@mail.com',
        },
        cellphone: {
          label: 'Cellphone:',
          name: 'Cellphone',
          placeholder: 'Ex. 0981333444',
        },
        permission: {
          label: 'Permission:',
          name: 'Permission',
        },
      },
      save: 'Save',
      remove: 'Remove',
      removeMessage: {
        title: 'Remove this',
        message: 'Are you sure?',
        yes: 'Yes',
        no: 'No',
      },
    },
  },
  menu: {
    list: {
      title: 'Menu',
    },
    addCategory: {
      title: 'Select a Option',
    },
    add: {
      navigationBarTitile: 'Add menu',
      fields: {
        name: {
          label: 'Name:',
          name: 'Name',
          placeholder: 'Ex. Salads',
        },
        description: {
          label: 'Description:',
          name: 'Description',
          placeholder: 'Ex. With tomato, etc.',
        },
        price: {
          label: 'Price:',
          name: 'Price',
          placeholder: 'Ex. 100',
        },
      },
      editImage: 'Click to change pic',
      save: 'Add menu',
      update: 'Update menu',
    },
  },
  forgetPasswordScreen: {
    title: 'Reset password',
    subtitle: 'Write your email to reset your password',
    email: 'Email:',
    emailPlaceholder: 'example: john@mail.com',
    restore: 'Restore',
    back: 'Back',
    fields: {
      email: 'Email',
    },
    errors: {
      'auth/invalid-email': 'User not found or is invalid',
      'auth/missing-android-pkg-name': 'An error occurred, try againg later',
      'auth/missing-continue-uri': 'An error occurred, try againg later',
      'auth/missing-ios-bundle-id': 'An error occurred, try againg later',
      'auth/invalid-continue-uri': 'An error occurred, try againg later',
      'auth/unauthorized-continue-uri': 'An error occurred, try againg later',
      'auth/user-not-found': 'User not found or is invalid',
      default: 'An error occurred, try againg later',
    },
  },
  tables: {
    list: {
      title: 'Tables',
      searchPlaceholder: 'Search table',
      table: 'Table',
      chairs: '%{chairs} Chairs',
      smokingArea: 'Smoking area: %{can}',
    },
    add: {
      navigationBarTitile: 'Add',
      fields: {
        tableNumber: {
          label: 'Table number:',
          name: 'Table number',
          placeholder: 'Ex. 01',
        },
        totalChairs: {
          label: 'Total chairs:',
          name: 'Total chairs',
          placeholder: 'Ex. 5',
        },
        smokingArea: {
          label: 'Smoking area',
          name: 'Smoking area',
          placeholder: '',
        },
      },
      save: 'Guardar',
    },
  },
  businessScreen: {
    add: {
      navigationBarTitile: 'Business',
      fields: {
        name: {
          label: 'Name:',
          name: 'Name',
          placeholder: 'Ex. Other Bar',
        },
        address: {
          label: 'Address:',
          name: 'Address',
          placeholder: 'Ex. First street 2045',
        },
        phone: {
          label: 'Phone:',
          name: 'Phone',
          placeholder: 'Ex. 224-344',
        },
      },
      save: 'Save',
    },
    update: {
      navigationBarTitile: 'Business',
      fields: {
        name: {
          label: 'Name:',
          name: 'Name',
          placeholder: 'Ex. Other Bar',
        },
        address: {
          label: 'Address:',
          name: 'Address',
          placeholder: 'Ex. First street 2045',
        },
        phone: {
          label: 'Phone:',
          name: 'Phone',
          placeholder: 'Ex. 224-344',
        },
      },
      save: 'Update',
    },
  },
  localsScreen: {
    list: {
      title: 'Locals',
      searchPlaceholder: 'Search',
      local: 'Local',
    },
    add: {
      navigationBarTitile: 'Add',
      fields: {
        name: {
          label: 'Name:',
          name: 'Name',
          placeholder: 'Ex. Brach offices one',
        },
        address: {
          label: 'Address:',
          name: 'Address',
          placeholder: 'Ex. Bulevar 20192',
        },
        phone: {
          label: 'Phone:',
          name: 'Phone',
          placeholder: 'Ex. 938211232',
        },
      },
      save: 'Add',
    },
    edit: {
      navigationBarTitile: 'Edit',
      fields: {
        name: {
          label: 'Name:',
          name: 'Name',
          placeholder: 'Ex. Brach offices one',
        },
        address: {
          label: 'Address:',
          name: 'Address',
          placeholder: 'Ex. Bulevar 20192',
        },
        phone: {
          label: 'Phone:',
          name: 'Phone',
          placeholder: 'Ex. 938211232',
        },
      },
      save: 'Update',
      remove: 'Remove',
      removeMessage: {
        title: 'Remove this',
        message: 'Are you sure?',
        yes: 'Yes',
        no: 'No',
      },
    },
  },
  menuOptionsScreen: {
    list: {
      title: 'Menu Options',
      searchPlaceholder: 'Search',
      local: 'Local',
    },
    add: {
      navigationBarTitile: 'Add',
      fields: {
        name: {
          label: 'Name:',
          name: 'Name',
          placeholder: 'Ex. Salads',
        },
      },
      save: 'Add',
    },
    edit: {
      navigationBarTitile: 'Edit',
      fields: {
        name: {
          label: 'Name:',
          name: 'Name',
          placeholder: 'Ex. Salads',
        },
      },
      save: 'Update',
      remove: 'Remove',
      removeMessage: {
        title: 'Remove this',
        message: 'Are you sure?',
        yes: 'Yes',
        no: 'No',
      },
    },
  },
};
